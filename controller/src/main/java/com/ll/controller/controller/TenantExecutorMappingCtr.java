package com.ll.controller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ll.controller.model.TenantExecutorMapping;
import com.ll.controller.service.TenantExecutorMappingService;

@RestController
@Controller
@RequestMapping(path = "/getTenant")
@CrossOrigin
public class TenantExecutorMappingCtr {
	@Autowired
	TenantExecutorMappingService service;
	
	@GetMapping()
	public ResponseEntity<List<TenantExecutorMapping>> get()	{

		return ResponseEntity.ok(this.service.getAll());
	}
}
