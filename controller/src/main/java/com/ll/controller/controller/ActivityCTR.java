package com.ll.controller.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ll.controller.model.Activity;
import com.ll.controller.service.ActivityService;

@RestController
@Controller
@RequestMapping(path = "/activity")
@CrossOrigin
public class ActivityCTR {
	
	@Autowired
	ActivityService service;
	
	@GetMapping()
	public ResponseEntity<List<Activity>> get()	{

		return ResponseEntity.ok(this.service.getActivityType());
	}

}