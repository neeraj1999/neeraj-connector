package com.ll.controller.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.ll.controller.model.Activity;
import com.ll.controller.repo.ActivityRepo;

@Service
@Component
public class ActivityService{
	
	@Autowired
	ActivityRepo repo;
	
	public List<Activity> getActivityType() {
		return repo.findAll();
	}
}
