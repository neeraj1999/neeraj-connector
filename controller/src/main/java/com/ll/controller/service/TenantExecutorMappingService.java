package com.ll.controller.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.ll.controller.model.TenantExecutorMapping;
import com.ll.controller.repo.TenantExecutorMappingRepo;

@Service
@Component
public class TenantExecutorMappingService {

	@Autowired
	TenantExecutorMappingRepo temRepo;
	
	public List<TenantExecutorMapping> getAll() {
		return temRepo.findAll();
	}
}
