package com.ll.controller;

import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@SpringBootApplication
@EnableScheduling
public class ControllerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControllerApplication.class, args);
		new ControllerApplication().task();
	}
	
	@Scheduled(cron = "1 * * * * *")
	public void task() {
		//String cron ="0 * 0 ? * * *";
		System.out.println("Time :" + new Date());
	}

}
