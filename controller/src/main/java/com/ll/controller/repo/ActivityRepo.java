package com.ll.controller.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ll.controller.model.Activity;

public interface ActivityRepo extends JpaRepository<Activity, Long>{
	

}