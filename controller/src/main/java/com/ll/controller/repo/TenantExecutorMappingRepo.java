package com.ll.controller.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ll.controller.model.TenantExecutorMapping;

public interface TenantExecutorMappingRepo extends JpaRepository<TenantExecutorMapping, Long>{

}
