package com.ll.controller.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "activity")
public class Activity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long activityID;

	@Column(name = "activity_name")
	private String activityName;

	@Column(name = "scope_id")
	private Long scopeID;

	@Column(name = "status")
	private String activityStatus;

	public Long getActivityID() {
		return activityID;
	}

	public void setActivityID(Long activityID) {
		this.activityID = activityID;
	}

	public String getActivityName() {
		return activityName;
	}

	public void setActivityName(String activityName) {
		this.activityName = activityName;
	}

	public Long getScopeID() {
		return scopeID;
	}

	public void setScopeID(Long scopeID) {
		this.scopeID = scopeID;
	}

	public String getActivityStatus() {
		return activityStatus;
	}

	public void setActivityStatus(String activityStatus) {
		this.activityStatus = activityStatus;
	}

	@Override
	public String toString() {
		return "Activity [activityID=" + activityID + ", activityName=" + activityName + ", scopeID=" + scopeID
				+ ", activityStatus=" + activityStatus + "]";
	}

	
}