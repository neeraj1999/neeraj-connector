package com.ll.controller.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "tenant_executor_mapping")
public class TenantExecutorMapping {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long tenantId;
	private long stationId;
	private long parameterId;
	private String executorName;
	private boolean isCron;
	private String cronExpression;
	private String fixedRate;

	public TenantExecutorMapping() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TenantExecutorMapping(long id, long tenantId, long stationId, long parameterId, String executorName,
			boolean isCron, String cronExpression, String fixedRate) {
		super();
		this.id = id;
		this.tenantId = tenantId;
		this.stationId = stationId;
		this.parameterId = parameterId;
		this.executorName = executorName;
		this.isCron = isCron;
		this.cronExpression = cronExpression;
		this.fixedRate = fixedRate;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTenantId() {
		return tenantId;
	}

	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}

	public long getStationId() {
		return stationId;
	}

	public void setStationId(long stationId) {
		this.stationId = stationId;
	}

	public long getParameterId() {
		return parameterId;
	}

	public void setParameterId(long parameterId) {
		this.parameterId = parameterId;
	}

	public String getExecutorName() {
		return executorName;
	}

	public void setExecutorName(String executorName) {
		this.executorName = executorName;
	}

	public boolean isCron() {
		return isCron;
	}

	public void setCron(boolean isCron) {
		this.isCron = isCron;
	}

	public String getFixedRate() {
		return fixedRate;
	}

	public void setFixedRate(String fixedRate) {
		this.fixedRate = fixedRate;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

}
