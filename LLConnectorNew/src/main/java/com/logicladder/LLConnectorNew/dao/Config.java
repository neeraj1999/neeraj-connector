package com.logicladder.LLConnectorNew.dao;

import java.util.List;

//import org.apache.commons.lang3.builder.ToStringBuilder;
//import org.apache.commons.lang3.builder.ToStringStyle;

public class Config {

	private List<PersistenceConfig> persistConfigs;
	private String applicationServer;

	public List<PersistenceConfig> getPersistConfigs() {
		return persistConfigs;
	}

	public String getApplicationServer() {
		return applicationServer;
	}

	public static class PersistenceConfig {
		public String name;
		public String unit;

//		@Override
//		public String toString() {
//			return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
//		}
	}

}
