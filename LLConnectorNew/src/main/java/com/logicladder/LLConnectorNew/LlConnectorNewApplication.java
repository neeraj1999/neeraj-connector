package com.logicladder.LLConnectorNew;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.logicladder.LLConnectorNew.app.Application;

@SpringBootApplication
public class LlConnectorNewApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(LlConnectorNewApplication.class, args);
		Application app = ctx.getBean(Application.class);
//		Application app = new Application();
	//	app.init();
		app.populateTable();
	}
	
	

}
