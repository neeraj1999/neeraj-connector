package com.logicladder.LLConnectorNew.helper;

import java.util.List;

import com.logicladder.LLConnectorNew.models.ParameterDetails;


public class ExecutorParameters {
	
	public ExecutorParameters(){
		
	}

	private String industryId;
	private String stationId;
	private String startTime;
	private String endTime;
	private String lastUpdatedDate;
	private List<ParameterDetails> paramList;
	
	
	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}
	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}
	public String getIndustryId() {
		return industryId;
	}
	public void setIndustryId(String industryId) {
		this.industryId = industryId;
	}
	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public List<ParameterDetails> getParamList() {
		return paramList;
	}
	public void setParamList(List<ParameterDetails> paramList) {
		this.paramList = paramList;
	}
	
}
