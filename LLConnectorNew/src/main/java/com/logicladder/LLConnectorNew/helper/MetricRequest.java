package com.logicladder.LLConnectorNew.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_EMPTY)
@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
public class MetricRequest {

	private List<RecentMetricQueries> queries = new ArrayList<MetricRequest.RecentMetricQueries>();
	private boolean msResolution = true;
	private String start;
	private String end;
	private String timezone = "IST";

	public MetricRequest(String start, String end) {
		this.start = start;
		this.end = end;
	}

	public void addQuery(String metricName, long deviceId, String downsample, String aggregator) {
		queries.add(new RecentMetricQueries(metricName, deviceId, downsample, aggregator));
	}

	@JsonAutoDetect(fieldVisibility = Visibility.ANY, getterVisibility = Visibility.NONE, setterVisibility = Visibility.NONE)
	public static class RecentMetricQueries {
		private String metric;
		private String aggregator;
		private String downsample;
		private Map<String, String> tags;

		public RecentMetricQueries(String metric, long deviceId,
				String downsample, String aggregator) {
			this.metric = metric;
			this.aggregator = aggregator;
			this.downsample = downsample;
			tags = new HashMap<String, String>();
			tags.put("deviceId", String.valueOf(deviceId));
		}
	}
}
