package com.logicladder.LLConnectorNew.helper;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RecentTsdbDataResponse {
	private String metric;
	private Map<String, Object> tags;
	private LinkedHashMap<String, String> dataPoints;
	private List aggregateTags;
	private String timestamp;
	private String value;
	
	@JsonProperty("dps")
	public LinkedHashMap<String, String> getDataPoints() {
		return dataPoints;
	}


	public List getAggregateTags() {
		return aggregateTags;
	}

	public void setAggregateTags(List aggregateTags) {
		this.aggregateTags = aggregateTags;
	}

	@JsonIgnore
	private String tsuid;

	public String getMetric() {
		return metric;
	}

	public Map<String, Object> getTags() {
		return tags;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getValue() {
		return value;
	}

	public String getTsuid() {
		return tsuid;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}

	public void setTags(Map<String, Object> tags) {
		this.tags = tags;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setTsuid(String tsuid) {
		this.tsuid = tsuid;
	}
	
	
}
