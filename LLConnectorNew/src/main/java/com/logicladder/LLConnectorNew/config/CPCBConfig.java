package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CPCBConfig extends CommonConfig {

	static final Logger logger = LoggerFactory.getLogger(CPCBConfig.class);
	private String propFileName = "config/LLCPCBConfig.properties";

	public CPCBConfig() {
		loadConfig();
	}

	public void loadConfig() {

		InputStream is = CPCBConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			logger.debug("inside loadConfig()");
			prop.load(is);
			
			postData = Boolean.parseBoolean(prop.getProperty("postData"));
			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			token = prop.getProperty("token");
			conversionRequired = Boolean.parseBoolean(prop.getProperty("conversionRequired"));
			industryUrl = prop.getProperty("industryUrl");
			stationUrl = prop.getProperty("stationUrl");
			DiagnosticIndustriesLogicladder = prop.getProperty("DiagnosticIndustriesLogicladder");
			diagnosticPostData = Boolean.parseBoolean(prop.getProperty("diagnosticPostData"));
			delayInfoInterval = prop.getProperty("delayInfoInterval");
			durationBreak = prop.getProperty("durationBreak");
			statutoryId = prop.getProperty("statutoryIdForCPCB");
			postUrl = prop.getProperty("urlCPCB");
			portNo = Integer.parseInt(prop.getProperty("portNumberCPCB"));
			sourceTsdbHost = prop.getProperty("sourceTsdbHost");
			sourceTsdbPort = Integer.parseInt(prop.getProperty("sourceTsdbPort"));
			sourceTsdbEndPoint = prop.getProperty("sourceTsdbEndPoint");
			offset = Integer.parseInt(prop.getProperty("offset"));
			String paramsStr = prop.getProperty("params");
			String[] params = paramsStr.split(",");
			for (String param : params) {
				paramKeyMap.put(param, prop.getProperty(param));
			}
			String unitStr = prop.getProperty("units");
			String[] units = unitStr.split(",");
			for (String unit : units) {
				unitKeyMap.put(unit, prop.getProperty(unit));
			}
			DiagParamIds = prop.getProperty("DiagParamIds");
			String[] diagParamId = DiagParamIds.split(",");
			for (String parameterId : diagParamId) {
				DiagnosticParamId.put(parameterId, prop.getProperty(parameterId));
			}
			DiagParam = prop.getProperty("diagnosticParam");
			String[] diagParam = DiagParam.split(",");
			for (String parameterName : diagParam) {
				DiagnosticParam.put(parameterName, prop.getProperty(parameterName));
			}
		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
			System.exit(0);
		}
	}

}
