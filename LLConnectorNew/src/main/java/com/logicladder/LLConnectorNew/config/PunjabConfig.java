package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PunjabConfig extends CommonConfig {

	static final Logger logger = LoggerFactory.getLogger(PunjabConfig.class);
	private String propFileName = "config/PunjabSPCBConfig.properties";

	public PunjabConfig() {
		loadConfig();
	}

	public void loadConfig() {

		InputStream is = PunjabConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		try {
			logger.debug("inside loadConfig()");
			prop.load(is);

			postData = Boolean.parseBoolean(prop.getProperty("postData"));
			postUrl = prop.getProperty("urlSPCB");
			portNo = Integer.parseInt(prop.getProperty("portNumberSPCB"));
			industryUrl = prop.getProperty("industryUrl");
			stationUrl = prop.getProperty("stationUrl");
			token = prop.getProperty("token");
			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			statutoryId = prop.getProperty("statutoryIdForCPCB");
			sourceTsdbHost = prop.getProperty("sourceTsdbHost");
			sourceTsdbPort = Integer.parseInt(prop.getProperty("sourceTsdbPort"));
			sourceTsdbEndPoint = prop.getProperty("sourceTsdbEndPoint");
			delayInfoInterval = prop.getProperty("delayInfoInterval");

		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
			System.exit(0);
		}
	}

}
