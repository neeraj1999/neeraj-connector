package com.logicladder.LLConnectorNew.config;

import java.util.HashMap;
import java.util.Map;

public abstract class CommonConfig {

	String YYYYMMDD_SLASH = "yyyy/MM/dd HH:mm:ss";
	String YYYYMMDD_HYPHEN = "yyyy-MM-dd HH:mm:ss";

	public String sourceTsdbHost;
	public int sourceTsdbPort;
	public String sourceTsdbEndPoint;
	public String sourceTsdbEndRecentPoint;

	public boolean postData;
	public String postUrl;
	public int portNo;
	public String token;
	public String schemeName;
	public boolean conversionRequired;

	public String processingFolder;
	public String processedFolder;
	public String realtimeUpload;
	public String delayedUpload;
	public String apiVersionID;
	public String fileUnencryptedExtension;
	public String fileEncryptedExtension;
	public String zipExtension;
	public String headerDelimiter;
	public String modulusServerKey;
	public String timeCheckForRealTimeAPI;
	public String fileExtension;

	public String statutoryId;
	public String industryUrl;
	public String stationUrl;

	public int sleepInterval;
	public String delayInfoUrl;
	public String delayInfoInterval;
	public String durationBreak;

	public String heartBeatUrl;
	public String heartBeatInterval;

	public int offset;

	public boolean diagnosticPostData;
	public String DiagParamIds;
	public String DiagParam;
	public String DiagnosticIndustriesLogicladder;

	public Map<String, String> paramKeyMap = new HashMap<String, String>();
	public Map<String, String> unitKeyMap = new HashMap<String, String>();
	public Map<String, String> DiagnosticParamId = new HashMap<String, String>();
	public Map<String, String> DiagnosticParam = new HashMap<String, String>();

	public abstract void loadConfig();

}
