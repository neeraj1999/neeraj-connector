package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TelanganaConfig extends CommonConfig {

	static final Logger logger = LoggerFactory.getLogger(TelanganaConfig.class);
	static private String propFileName = "config/LLTSPCBConfig.properties";
	public String postUrlTSPCB;
	public int portNoTSPCB;
	public String industryUrl;
	public String stationUrl;
	public boolean dataRange;
	public String startDate;
	public String endDate;
	public int dataFetchLimitFromDB;
	public int offset;
	public String delayInfoUrl;
	public String delayInfoInterval;
	public String heartBeatUrl;
	public String heartBeatInterval;
	public int threadCount;
	public String destTsdbHost;
	public int destTsdbPort;
	public boolean isConversion;
	public String userTSPCB;
	public String pwdTSPCB;
	public String stationType;
	public String apiUrlTSPCB;
	public String enviroLogicDemoIndustries;
	public Map<String, String> DeviceIdsTSPCB = new HashMap<String, String>();
	public String telanganaIndustries;
	public String DiagParamIds;
	public String statutoryIdForCPCB;
	public String softwareVersion;
	public String tspcbUsers;
	public Map<String, String> tspcbUserMap = new HashMap<String, String>();
	public String entityRoles_domain;
	public String devicesStationType;
	public Map<String, String> paramList = new HashMap<String, String>();
	public boolean flagForLatestRequestDelay;
	public int delayIntervalInMin;
	public boolean postAPIResponseToEnviro;
	public String postUrlEnvirologicIQ;
	public int portEnvirologicIQ;
	public String responseApiEndPointEnvirologicIQ;
	public String apiTokenEnvirologicIQ;

	public TelanganaConfig() {
		loadConfig();
	}

	public void loadConfig() {

		InputStream is = TelanganaConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			logger.debug("inside loadConfig()");
			prop.load(is);

			flagForLatestRequestDelay = Boolean.parseBoolean(prop.getProperty("flagForLatestRequestDelay"));
			delayIntervalInMin = Integer.parseInt(prop.getProperty("delayIntervalInMin"));
			postData = Boolean.parseBoolean(prop.getProperty("postData"));
			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			stationType = prop.getProperty("stationType");
			token = prop.getProperty("token");
			industryUrl = prop.getProperty("industryUrl");
			stationUrl = prop.getProperty("stationUrl");
			delayInfoUrl = prop.getProperty("delayInfoUrl");
			delayInfoInterval = prop.getProperty("delayInfoInterval");
			heartBeatUrl = prop.getProperty("heartBeatUrl");
			heartBeatInterval = prop.getProperty("heartBeatInterval");
			threadCount = Integer.parseInt(prop.getProperty("noOfThread"));
			offset = Integer.parseInt(prop.getProperty("offset"));
			dataRange = Boolean.parseBoolean(prop.getProperty("dataRange").trim());
			startDate = prop.getProperty("startDate");
			endDate = prop.getProperty("endDate");

			enviroLogicDemoIndustries = prop.getProperty("enviroLogicDemoIndustries");
			statutoryIdForCPCB = prop.getProperty("statutoryIdForCPCB");
			entityRoles_domain = prop.getProperty("entityRoles_domain");
			devicesStationType = prop.getProperty("devicesStationType");

			dataFetchLimitFromDB = Integer.parseInt(prop.getProperty("dataFetchLimitFromDB"));

			sourceTsdbHost = prop.getProperty("sourceTsdbHost");
			sourceTsdbPort = Integer.parseInt(prop.getProperty("sourceTsdbPort"));
			sourceTsdbEndPoint = prop.getProperty("sourceTsdbEndPoint");
			sourceTsdbEndRecentPoint = prop.getProperty("sourceTsdbRecentEndPoint");
			destTsdbHost = prop.getProperty("destinationTsdbHost");
			destTsdbPort = Integer.parseInt(prop.getProperty("destinationtsdbPort"));
			postAPIResponseToEnviro = Boolean.parseBoolean(prop.getProperty("postAPIResponseToEnviro"));
			postUrlEnvirologicIQ = prop.getProperty("postUrlEnvirologicIQ");
			portEnvirologicIQ = Integer.parseInt(prop.getProperty("portEnvirologicIQ"));
			responseApiEndPointEnvirologicIQ = prop.getProperty("responseApiEndPointEnvirologicIQ");
			apiTokenEnvirologicIQ = prop.getProperty("apiTokenEnvirologicIQ");
			statutoryId = prop.getProperty("statutoryId");
			String paramsStr = prop.getProperty("params");
			String[] params = paramsStr.split(",");
			for (String param : params) {
				paramKeyMap.put(param, prop.getProperty(param));
			}

			String unitStr = prop.getProperty("units");
			String[] units = unitStr.split(",");
			for (String unit : units) {
				unitKeyMap.put(unit, prop.getProperty(unit));
			}

			String prm = prop.getProperty("paramList");
			String[] parameters = prm.split(",");
			for (String pram : parameters) {
				paramList.put(pram, prop.getProperty(pram));
			}

			postUrlTSPCB = prop.getProperty("urlTSPCB");
			portNoTSPCB = Integer.parseInt(prop.getProperty("portNumberTSPCB"));
			apiUrlTSPCB = prop.getProperty("apiUrlTSPCB");
			telanganaIndustries = prop.getProperty("telanganaIndustries");
			softwareVersion = prop.getProperty("softwareVersion");

			String Devices = prop.getProperty("DeviceIdsTSPCB");
			String[] DeviceIds = Devices.split(",");
			for (String ids : DeviceIds) {
				DeviceIdsTSPCB.put(ids, prop.getProperty(ids));
			}

			tspcbUsers = prop.getProperty("tspcbUsers");
			String[] users = tspcbUsers.split(",");
			for (String user : users) {
				tspcbUserMap.put(user, prop.getProperty(user));
			}

			

			logger.info("Reading PostUrlTSPCB {}", postUrlTSPCB);
			logger.info("Reading portNoTSPCB {}", portNoTSPCB);
			logger.info("Reading postData {}", postData);
			logger.info("Reading sleepInterval {}", sleepInterval);
			logger.info("parameter mapping {}", paramKeyMap);
			logger.info("unit mapping {}", unitKeyMap);

		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
			System.exit(0);
		}
	}

}
