package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JharkhandConfig extends CommonConfig {

	static final Logger logger = LoggerFactory.getLogger(JharkhandConfig.class);
	private String propFileName = "config/JharkhandConfig.properties";

	public JharkhandConfig() {
		loadConfig();
	}

	public void loadConfig() {

		InputStream is = JharkhandConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			logger.debug("inside loadConfig()");
			prop.load(is);
			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			postData = Boolean.parseBoolean(prop.getProperty("postData"));
			postUrl = prop.getProperty("postUrl");
			portNo = Integer.parseInt(prop.getProperty("portNo").toString());
			sourceTsdbHost = prop.getProperty("sourceTsdbHost");
			sourceTsdbPort = Integer.parseInt(prop.getProperty("sourceTsdbPort"));
			sourceTsdbEndPoint = prop.getProperty("sourceTsdbEndPoint");
			statutoryId = prop.getProperty("statutoryId");
			delayInfoInterval = prop.getProperty("delayInfoInterval");
			stationUrl = prop.getProperty("stationUrl");

			String paramsStr = prop.getProperty("params");
			String[] params = paramsStr.split(",");
			for (String param : params) {
				paramKeyMap.put(param, prop.getProperty(param));
			}
			String unitStr = prop.getProperty("units");
			String[] units = unitStr.split(",");
			for (String unit : units) {
				unitKeyMap.put(unit, prop.getProperty(unit));
			}

		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
			System.exit(0);
		}
	}

}