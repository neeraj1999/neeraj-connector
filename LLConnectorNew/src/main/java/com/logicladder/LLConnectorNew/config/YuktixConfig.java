package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class YuktixConfig extends YutixSpecificConfig {

	static final Logger logger = LoggerFactory.getLogger(YuktixConfig.class);
	private String propFileName = "config/YuktixConfig.properties";

	public YuktixConfig() {
		loadConfig();
	}

	public void loadConfig() {

		InputStream is = YuktixConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			logger.debug("Yuktix inside loadConfig()");
			prop.load(is);
			yuktixURL = prop.getProperty("yuktixURL");
			portNo = Integer.parseInt(prop.getProperty("yuktixPortNo"));
			yuktixPostRequestFlag = Boolean.parseBoolean(prop.getProperty("yuktixPostRequestFlag"));
			yuktixAPIUrlForTimeRange = prop.getProperty("yuktixAPIUrlForTimeRange");

			enviroURL = prop.getProperty("enviroURL");
			enviroPostRequestFlag = Boolean.parseBoolean(prop.getProperty("enviroPostRequestFlag"));
			enviroAPIUrl = prop.getProperty("enviroAPIUrl");
			;

			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			noOfThread = Integer.parseInt(prop.getProperty("noOfThread"));
			ArrayBlockingQueue = Integer.parseInt(prop.getProperty("ArrayBlockingQueue"));

			delayInfoUrl = prop.getProperty("delayInfoUrl");
			delayInfoInterval = prop.getProperty("delayInfoInterval");

			statCron = prop.getProperty("statCron");

			startDate = prop.getProperty("startDate");
			endDate = prop.getProperty("endDate");

			String paramsStr = prop.getProperty("params");
			String[] params = paramsStr.split(",");
			for (String param : params) {
				paramKeyMap.put(param, prop.getProperty(param));
			}

			String unitStr = prop.getProperty("units");
			String[] units = unitStr.split(",");
			for (String unit : units) {
				unitKeyMap.put(unit, prop.getProperty(unit));
			}

			String stationStr = prop.getProperty("stations");
			stations = stationStr.split(",");
			for (String station : stations) {
				stationsMap.put(station, prop.getProperty(station));
			}
		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
		}
	}

}
