package com.logicladder.LLConnectorNew.config;

public class ConfigFactory {

	private ConfigFactory() {

	}

	public static ConfigFactory getInstance() {
		return new ConfigFactory();
	}

	public CommonConfig getConfig(String regulatoryCode) {

		switch (regulatoryCode) {

		case "HARYANA":
			return new HaryanaConfig();

		case "DELHI":
			return new DelhiConfig();

		case "BIHAR":
			return new BiharConfig();

		case "MAHARASHTRA":
			return new MPCBConfig();

		case "ANDHRA":
			return new AndhraConfig();

		case "CPCB":
			return new CPCBConfig();

		case "KARNATAKA":
			return new CPCBConfig();

		case "DEMO":
			return new DemoConfig();

		case "GMDA":
			return new GMDAConfig();

		case "KERALA":
			return new keralaConfig();

		case "ORISSA":
			return new OrissaConfig();

		case "RAJASTHAN":
			return new RajasthanConfig();

		case "TELANGANA":
			return new TelanganaConfig();

		case "JHARKHAND":
			return new JharkhandConfig();

		case "MADHYAPRADESH":
			return new MadhyaPradeshConfig();

		case "HARYANA_ANODYNE":
			return new AnodyneConfig();

		case "DELHI_ANODYNE":
			return new DelhiAnodyneConfig();

		case "PUNJAB":
			return new PunjabConfig();

		case "GUJARAT":
			return new GujaratConfig();

		case "ZEMA":
			return new ZemaConfig();
		default:
			return null;
		}
	}

}
