package com.logicladder.LLConnectorNew.config;

import java.util.HashMap;
import java.util.Map;

public abstract class YutixSpecificConfig extends CommonConfig {

	protected String yuktixURL;
	protected boolean yuktixPostRequestFlag;
	protected String yuktixAPIUrlForTimeRange;
	protected String[] stations;
	protected String enviroURL;
	protected boolean enviroPostRequestFlag;
	protected String enviroAPIUrl;
	protected int noOfThread;
	protected int ArrayBlockingQueue;
	public String startDate;
	public String endDate;
	
	protected Map<String, String> stationsMap = new HashMap<String, String>();

	protected String statCron;

	public String getYuktixURL() {
		return yuktixURL;
	}

	public boolean isYuktixPostRequestFlag() {
		return yuktixPostRequestFlag;
	}

	public String getYuktixAPIUrlForTimeRange() {
		return yuktixAPIUrlForTimeRange;
	}

	public String[] getStations() {
		return stations;
	}

	public String getEnviroURL() {
		return enviroURL;
	}

	public boolean isEnviroPostRequestFlag() {
		return enviroPostRequestFlag;
	}

	public String getEnviroAPIUrl() {
		return enviroAPIUrl;
	}

	public int getNoOfThread() {
		return noOfThread;
	}

	public int getArrayBlockingQueue() {
		return ArrayBlockingQueue;
	}

	public Map<String, String> getStationsMap() {
		return stationsMap;
	}

	public String getStatCron() {
		return statCron;
	}

}
