package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GMDAConfig extends CommonConfig {

	static final Logger logger = LoggerFactory.getLogger(GMDAConfig.class);
	private String propFileName = "config/GMDAConfig.properties";

	public GMDAConfig() {
		loadConfig();
	}

	public void loadConfig() {

		InputStream is = GMDAConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			logger.debug("inside loadConfig()");
			prop.load(is);
			postData = Boolean.parseBoolean(prop.getProperty("postData"));
			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			token = prop.getProperty("token");
			stationUrl = prop.getProperty("stationUrl");
			delayInfoInterval = prop.getProperty("delayInfoInterval");

			statutoryId = prop.getProperty("statutoryIdForCPCB");

			postUrl = prop.getProperty("urlCPCB");
			portNo = Integer.parseInt(prop.getProperty("portNumberCPCB"));

			sourceTsdbHost = prop.getProperty("sourceTsdbHost");
			sourceTsdbPort = Integer.parseInt(prop.getProperty("sourceTsdbPort"));
			sourceTsdbEndPoint = prop.getProperty("sourceTsdbEndPoint");

			String paramsStr = prop.getProperty("params");
			String[] params = paramsStr.split(",");
			for (String param : params) {
				paramKeyMap.put(param, prop.getProperty(param));
			}

			String unitStr = prop.getProperty("units");
			String[] units = unitStr.split(",");
			for (String unit : units) {
				unitKeyMap.put(unit, prop.getProperty(unit));
			}

		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
			System.exit(0);
		}
	}

}
