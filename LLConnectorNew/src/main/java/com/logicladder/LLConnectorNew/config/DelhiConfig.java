package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DelhiConfig extends CommonConfig {

	static final Logger logger = LoggerFactory.getLogger(DelhiConfig.class);
	private String propFileName = "config/DelhiSPCBConfig.properties";

	public DelhiConfig() {
		loadConfig();
	}

	public void loadConfig() {

		InputStream is = HaryanaConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));

		try {
			logger.debug("inside loadConfig()");
			prop.load(is);
			postData = Boolean.parseBoolean(prop.getProperty("postData"));
			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			token = prop.getProperty("token");
			industryUrl = prop.getProperty("industryUrl");
			stationUrl = prop.getProperty("stationUrl");
			DiagnosticIndustriesLogicladder = prop.getProperty("DiagnosticIndustriesLogicladder");
			diagnosticPostData = Boolean.parseBoolean(prop.getProperty("diagnosticPostData"));
			delayInfoUrl = prop.getProperty("delayInfoUrl");
			delayInfoInterval = prop.getProperty("delayInfoInterval");
			statutoryId = prop.getProperty("statutoryIdForCPCB");

			postUrl = prop.getProperty("urlCPCB");
			portNo = Integer.parseInt(prop.getProperty("portNumberCPCB"));
			schemeName=prop.getProperty("schemeName");

			sourceTsdbHost = prop.getProperty("sourceTsdbHost");
			sourceTsdbPort = Integer.parseInt(prop.getProperty("sourceTsdbPort"));
			sourceTsdbEndPoint = prop.getProperty("sourceTsdbEndPoint");

		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
			System.exit(0);
		}
	}

}
