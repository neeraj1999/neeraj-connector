package com.logicladder.LLConnectorNew.config;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Properties;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AndhraConfig extends CommonConfig {

	static final Logger logger = LoggerFactory.getLogger(AndhraConfig.class);
	private String propFileName = "config/AndhraSPCBConfig.properties";

	public AndhraConfig() {
		loadConfig();
	}

	public void loadConfig() {
		InputStream is = AndhraConfig.class.getClassLoader().getResourceAsStream(propFileName);
		Properties prop = new Properties();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("IST"));
		try {

			logger.debug("inside loadConfig()");
			prop.load(is);

			postData = Boolean.parseBoolean(prop.getProperty("postData"));
			statutoryId = prop.getProperty("statutoryId");
			delayInfoInterval = prop.getProperty("delayInfoInterval");
			sleepInterval = Integer.parseInt(prop.getProperty("sleepInterval"));
			postUrl = prop.getProperty("postUrl");
			durationBreak = prop.getProperty("durationBreak");
			portNo = Integer.parseInt(prop.getProperty("portNo").toString());
			processingFolder = prop.getProperty("processingFolder");
			processedFolder = prop.getProperty("processedFolder");
			realtimeUpload = prop.getProperty("realtimeUpload");
			delayedUpload = prop.getProperty("delayedUpload");
			apiVersionID = prop.getProperty("apiVersionID");
			sourceTsdbHost = prop.getProperty("sourceTsdbHost");
			sourceTsdbPort = Integer.parseInt(prop.getProperty("sourceTsdbPort"));
			sourceTsdbEndPoint = prop.getProperty("sourceTsdbEndPoint");
			fileUnencryptedExtension = prop.getProperty("fileUnencryptedExtension");
			fileEncryptedExtension = prop.getProperty("fileEncryptedExtension");
			zipExtension = prop.getProperty("zipExtension");
			timeCheckForRealTimeAPI = prop.getProperty("timeCheckForRealTimeAPI");

			String paramsStr = prop.getProperty("params");
			String[] params = paramsStr.split(",");
			for (String param : params) {
				paramKeyMap.put(param, prop.getProperty(param));
			}

		} catch (IOException e) {
			logger.error("error loading config", e.getMessage());
		}
	}

}
