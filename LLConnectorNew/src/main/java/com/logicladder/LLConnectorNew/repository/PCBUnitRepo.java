package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.PCBUnit;

public interface PCBUnitRepo extends JpaRepository<PCBUnit, Long>{

}
