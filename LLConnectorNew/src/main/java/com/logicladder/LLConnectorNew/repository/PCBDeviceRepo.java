package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.PCBDevice;

public interface PCBDeviceRepo extends JpaRepository<PCBDevice, Long> {

}
