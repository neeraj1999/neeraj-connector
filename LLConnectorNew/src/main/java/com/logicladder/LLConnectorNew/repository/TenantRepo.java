package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.logicladder.LLConnectorNew.models.Tenant;

@Repository
public interface TenantRepo extends JpaRepository<Tenant, Long>{

}
