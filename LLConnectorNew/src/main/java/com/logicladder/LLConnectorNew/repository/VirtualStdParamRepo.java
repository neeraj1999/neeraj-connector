package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.VirtualStdParam;

public interface VirtualStdParamRepo extends JpaRepository<VirtualStdParam, Long>{

}
