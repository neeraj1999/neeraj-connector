package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.PCBEntity;

public interface PCBEntityRepo extends JpaRepository<PCBEntity, Long>{

}
