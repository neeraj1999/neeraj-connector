package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.ModelStdParam;

public interface ModelStdParamRepo extends JpaRepository<ModelStdParam, Long>{

}
