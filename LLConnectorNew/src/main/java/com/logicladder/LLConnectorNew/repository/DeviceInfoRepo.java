package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.DeviceInfo;

public interface DeviceInfoRepo extends JpaRepository<DeviceInfo, Long>{

}
