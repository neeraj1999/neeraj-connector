package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.TenantAddress;

public interface TenantAddressRepo extends JpaRepository<TenantAddress, Long>{

}
