package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.Country;

public interface CountryRepo extends JpaRepository<Country, String> {

}
