package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.DeviceStdParam;

public interface DeviceStdParamRepo extends JpaRepository<DeviceStdParam, Long> {

}
