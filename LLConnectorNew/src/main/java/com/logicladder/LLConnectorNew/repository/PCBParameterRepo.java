package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.PCBParameter;

public interface PCBParameterRepo extends JpaRepository<PCBParameter, Long> {

}
