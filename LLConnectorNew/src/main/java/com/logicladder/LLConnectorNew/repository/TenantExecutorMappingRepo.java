package com.logicladder.LLConnectorNew.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.logicladder.LLConnectorNew.models.TenantExecutorMapping;

@Repository
public interface TenantExecutorMappingRepo extends JpaRepository<TenantExecutorMapping, Long>{

//	@Query("select tem.id,tem.tenantId,tem.stationId,tem.statutoryId,tem.executorName,tem.cronExpression,tem.regulatoryCode from TenantExecutorMapping tem where cronExpression=?1")
//	public List<TenantExecutorMapping> getTenantExecutorMappingByCron (String cron);
	
	@Query("select distinct cronExpression from TenantExecutorMapping")
	public List<String> getDistinctCronExpression();

	public List<TenantExecutorMapping> findAllByCronExpression(String cron);
	
	@Query("select distinct statutoryId from TenantExecutorMapping")
	public List<Long> getDistinctStatIds();
	
	@Query("select distinct regulatoryCode from TenantExecutorMapping where statutoryId=?1")
	public String findRegulatoryCodeByStatutoryId(Long statId);
}
