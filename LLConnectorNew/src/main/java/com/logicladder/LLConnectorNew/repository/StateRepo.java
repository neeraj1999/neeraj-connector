package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.State;

public interface StateRepo extends JpaRepository<State, String>{

}
