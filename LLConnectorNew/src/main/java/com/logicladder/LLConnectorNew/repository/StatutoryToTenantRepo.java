package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.StatutoryToTenant;

public interface StatutoryToTenantRepo extends JpaRepository<StatutoryToTenant, Long> {

}
