package com.logicladder.LLConnectorNew.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.logicladder.LLConnectorNew.models.EntityStdParam;

public interface EntityStdParamRepo extends JpaRepository<EntityStdParam, Long> {

}
