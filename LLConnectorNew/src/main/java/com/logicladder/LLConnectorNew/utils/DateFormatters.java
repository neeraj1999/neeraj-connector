package com.logicladder.LLConnectorNew.utils;

import java.text.SimpleDateFormat;

public class DateFormatters {

	public static SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");
	public static SimpleDateFormat yyyyMMddTHHmmssZ = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
	protected static String YYYYMMDD_SLASH = "yyyy/MM/dd HH:mm:ss";
	protected static String YYYYMMDD_HYPHEN = "yyyy-MM-dd HH:mm:ss";
	public static SimpleDateFormat dataPointFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
	public static SimpleDateFormat tsdbDateFormat = new SimpleDateFormat(YYYYMMDD_HYPHEN);
	public static SimpleDateFormat yy = new SimpleDateFormat("yy");
	public static SimpleDateFormat M = new SimpleDateFormat("M");
	public static SimpleDateFormat d = new SimpleDateFormat("d");
	public static SimpleDateFormat H = new SimpleDateFormat("H");
	public static SimpleDateFormat m = new SimpleDateFormat("m");
	public static SimpleDateFormat s = new SimpleDateFormat("s");
	
	
}
