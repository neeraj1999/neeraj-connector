package com.logicladder.LLConnectorNew.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {

	public static String YYYYMMDD_SLASH = "yyyy/MM/dd HH:mm:ss";
	public static String YYYYMMDD_HYPHEN = "yyyy-MM-dd HH:mm:ss";

	private static ThreadLocal<SimpleDateFormat> yyyyMMddHHmmssHolder = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMddHHmmss");
		}
	};
	private static ThreadLocal<SimpleDateFormat> dataPointFormatHolder = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(YYYYMMDD_HYPHEN);
		}
	};
	private static ThreadLocal<SimpleDateFormat> tsdbDateFormatHolder = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(YYYYMMDD_SLASH);
		}
	};
	private static ThreadLocal<SimpleDateFormat> headerDateFormatHolder = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		}
	};
	private static ThreadLocal<SimpleDateFormat> headerDateFormatHolderWithZeroSeconds = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:00'Z'");
		}
	};

	public static String format(long timeStamp, String dateFormat) {
		if (timeStamp != 0) {
			if (dateFormat.equals("yyyyMMddHHmmss")) {
				yyyyMMddHHmmssHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return yyyyMMddHHmmssHolder.get().format(timeStamp);
			}
			if (dateFormat.equals(YYYYMMDD_HYPHEN)) {
				dataPointFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return dataPointFormatHolder.get().format(timeStamp);
			}
			if (dateFormat.equals(YYYYMMDD_SLASH)) {
				tsdbDateFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return tsdbDateFormatHolder.get().format(timeStamp);
			}
			if (dateFormat.equals("yyyy-MM-dd'T'HH:mm:ss'Z'")) {
				headerDateFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return headerDateFormatHolder.get().format(timeStamp);
			}
			if (dateFormat.equals("yyyy-MM-dd'T'HH:mm:00'Z'")) {
				headerDateFormatHolderWithZeroSeconds.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return headerDateFormatHolderWithZeroSeconds.get().format(timeStamp);
			}

		}
		return null;
	}

	public static String format(Date dt, String dateFormat) {

		if (dt != null) {
			if (dateFormat.equals("yyyyMMddHHmmss")) {
				yyyyMMddHHmmssHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return yyyyMMddHHmmssHolder.get().format(dt);
			}
			if (dateFormat.equals("yyyy-MM-dd'T'HH:mm:ss'Z'")) {
				headerDateFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return headerDateFormatHolder.get().format(dt);
			}
			if (dateFormat.equals(YYYYMMDD_HYPHEN)) {
				dataPointFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return dataPointFormatHolder.get().format(dt);
			}
			if (dateFormat.equals(YYYYMMDD_SLASH)) {
				tsdbDateFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
				return tsdbDateFormatHolder.get().format(dt);
			}
		}
		return null;
	}

	public static Date parse(String dt, String dateFormat){
		try {
			if (dt != null) {
				if (dateFormat.equals("yyyyMMddHHmmss")) {
					yyyyMMddHHmmssHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
					return yyyyMMddHHmmssHolder.get().parse(dt);
				}
				if (dateFormat.equals("yyyy-MM-dd'T'HH:mm:ss'Z'")) {
					headerDateFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
					return headerDateFormatHolder.get().parse(dt);
				}
				if (dateFormat.equals(YYYYMMDD_HYPHEN)) {
					dataPointFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
					return dataPointFormatHolder.get().parse(dt);
				}
				if (dateFormat.equals(YYYYMMDD_SLASH)) {
					tsdbDateFormatHolder.get().setTimeZone(TimeZone.getTimeZone("IST"));
					return tsdbDateFormatHolder.get().parse(dt);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String convert(String dt, String sourceDateFormat, String dstnDateFormat) throws ParseException {

		Date date = null;
		String convertedDate = null;
		if (sourceDateFormat.equals(YYYYMMDD_HYPHEN)) {
			date = dataPointFormatHolder.get().parse(dt);
		}
		if (sourceDateFormat.equals(YYYYMMDD_SLASH)) {
			date = tsdbDateFormatHolder.get().parse(dt);
		}
		if (dstnDateFormat.equals(YYYYMMDD_SLASH)) {
			convertedDate = tsdbDateFormatHolder.get().format(date);
		}
		if (dstnDateFormat.equals(YYYYMMDD_HYPHEN)) {
			convertedDate = dataPointFormatHolder.get().format(date);
		}
		return convertedDate;
		
	}
	
	public static Date add(Date date,int hours,int minutes,int seconds,int millis){
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("IST"));
		cal.setTime(date);
		cal.add(Calendar.HOUR_OF_DAY, hours);
        cal.add(Calendar.MINUTE, minutes);
		cal.add(Calendar.SECOND, seconds);
		cal.add(Calendar.MILLISECOND,millis);
		Date modifiedDate = cal.getTime();
		return modifiedDate;
	}
	
	
	public static Date getCurrentDateTime(){
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("IST"));
		cal.setTimeInMillis(System.currentTimeMillis());
		return cal.getTime();
	}
	
	
	public static double convertToSI(double conversionFactor, double constant, double value){
		return value/conversionFactor - constant;
	}
	
	public static double convertFromSI(double conversiontFactor, double constant, double value){
		return value*conversiontFactor + constant;
	}
	
	public static double convertToSI(double conversionFactor, double constant, int value){
		return value/conversionFactor - constant;
	}
	
	public static double convertFromSI(double conversiontFactor, double constant, int value){
		return value*conversiontFactor + constant;
	}
	
	public static double convertToSI(double conversionFactor, double constant, long value){
		return value/conversionFactor - constant;
	}
	
	public static double convertFromSI(double conversiontFactor, double constant, long value){
		return value*conversiontFactor + constant;
	}
	
	public static boolean looksLikeInteger(final String value) {
	    final int n = value.length();
	    for (int i = 0; i < n; i++) {
	      final char c = value.charAt(i);
	      if (c == '.' || c == 'e' || c == 'E') {
	        return false;
	      }
	    }
	    return true;
	  }
	
	public static long UTCtoIST(long timeinMilis){
		TimeZone tz = TimeZone.getTimeZone("IST");
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(tz);
		cal.setTimeInMillis(timeinMilis);
		return cal.getTimeInMillis()+TimeZone.getTimeZone("IST").getOffset(timeinMilis);
	}
	
	public static double convertUnit(double sourceConversionFactor, double sourceConstant, boolean considerSrcMolWt, double value, double destConversionFactor, double destConstant, boolean considerDestMolWt, Double molecularWt) {
		value = convertToSI(sourceConversionFactor, sourceConstant, value, considerSrcMolWt, molecularWt);
		return convertFromSI(destConversionFactor, destConstant, value, considerDestMolWt, molecularWt);
	}
	
	public static double convertUnit(double sourceConversionFactor, double sourceConstant, boolean considerSrcMolWt, int value, double destConversionFactor, double destConstant, boolean considerDestMolWt, Double molecularWt) {
		double valueD =  convertToSI(sourceConversionFactor, sourceConstant, value, considerSrcMolWt, molecularWt);
		return convertFromSI(destConversionFactor, destConstant, valueD, considerDestMolWt, molecularWt);
	}
	
	public static double convertUnit(double sourceConversionFactor, double sourceConstant, boolean considerSrcMolWt, long value, double destConversionFactor, double destConstant, boolean considerDestMolWt, Double molecularWt) {
		double valueD =  convertToSI(sourceConversionFactor, sourceConstant, value, considerSrcMolWt, molecularWt);
		return convertFromSI(destConversionFactor, destConstant, valueD, considerDestMolWt, molecularWt);
	}
	
	
	public static double convertToSI(double conversionFactor, double constant, double value, boolean considerMolWt, Double molecularWt){
		if(considerMolWt && molecularWt != null){
			return value*molecularWt/conversionFactor - constant;
		} else {
			return convertToSI(conversionFactor, constant, value);
		}
		
	}
	
	public static double convertToSI(double conversionFactor, double constant, int value, boolean considerMolWt, Double molecularWt){
		if(considerMolWt && molecularWt != null){
			return convertToSI(conversionFactor, constant, (double)value, considerMolWt, molecularWt);
		} else {
			return convertToSI(conversionFactor, constant, value);
		}
	}
	
	public static double convertToSI(double conversionFactor, double constant, long value, boolean considerMolWt, Double molecularWt){
		if(considerMolWt && molecularWt != null){
			return convertToSI(conversionFactor, constant, (double)value, considerMolWt, molecularWt);
		} else {
			return convertToSI(conversionFactor, constant, value);
		}
	}
	
	
	
	public static double convertFromSI(double conversiontFactor, double constant, double value, boolean considerMolWt, Double molecularWt){
		if(considerMolWt && molecularWt != null) {
			return value*conversiontFactor/molecularWt + constant;
		} else {
			return convertFromSI(conversiontFactor, constant, value);
		}
		
	}
	
	public static double convertFromSI(double conversiontFactor, double constant, int value, boolean considerMolWt, Double molecularWt){
		if(considerMolWt && molecularWt != null) {
			return convertFromSI(conversiontFactor, constant, (double)value, considerMolWt, molecularWt);
		} else {
			return convertFromSI(conversiontFactor, constant, value);
		}
	}
	
	public static double convertFromSI(double conversiontFactor, double constant, long value, boolean considerMolWt, Double molecularWt){
		if(considerMolWt && molecularWt != null) {
			return convertFromSI(conversiontFactor, constant, (double)value, considerMolWt, molecularWt);
		} else {
			return convertFromSI(conversiontFactor, constant, value);
		}
	}
	

}
