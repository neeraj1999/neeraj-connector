package com.logicladder.LLConnectorNew.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.logicladder.LLConnectorNew.job.RunLiveApplicationJob;
import com.logicladder.LLConnectorNew.models.RegulatoryCode;
import com.logicladder.LLConnectorNew.models.TenantExecutorMapping;
import com.logicladder.LLConnectorNew.repository.PopulateTableRepo;
import com.logicladder.LLConnectorNew.repository.TenantExecutorMappingRepo;

@Component
public class Application {
	private static final Logger logger = LoggerFactory.getLogger(Application.class);
	
	@Autowired
	TenantExecutorMappingRepo temRepo;
	
	@Autowired
	PopulateTableRepo repo;
	
	@Autowired
	Scheduler scheduler;

	private JobKey runLiveAppJobKey;
	
	public Application() {
		super();
	}

	private void scheduleLiveJob(Map<String, List<TenantExecutorMapping>> tenantExecutorMappingMap) {
		for (Map.Entry<String, List<TenantExecutorMapping>> entry : tenantExecutorMappingMap.entrySet()) {
			JobDetail jobDetails = getJobDetails(entry);
			Trigger trigger = getTrigger(jobDetails);
			try {
				scheduler.scheduleJob(jobDetails, trigger);
			} catch (SchedulerException e) {
				logger.error("error in scheduling job {}",e);
				e.printStackTrace();
			}
		}
	}

	public void init() {
		List<String> distinctCronExpression = temRepo.getDistinctCronExpression();
		Map<String, List<TenantExecutorMapping>> tenantExecutorMappingMap = new HashMap<String, List<TenantExecutorMapping>>();
		for (String cron : distinctCronExpression) {
			logger.debug("getting List of tenantExecutorMapping by : {}", cron);
			List<TenantExecutorMapping> tenantExecutorMappingByCron = temRepo.findAllByCronExpression(cron);
			tenantExecutorMappingMap.put(cron, tenantExecutorMappingByCron);
			scheduleLiveJob(tenantExecutorMappingMap);
		}
	}

	private JobDetail getJobDetails(Entry<String, List<TenantExecutorMapping>> entry) {
		JobDataMap map = new JobDataMap();
		map.put("cronExpression", entry.getKey());
		map.put("mappingList", entry.getValue());
		return JobBuilder.newJob(RunLiveApplicationJob.class).withIdentity(runLiveAppJobKey)
				.usingJobData(map).storeDurably().build();
	}

	private Trigger getTrigger(JobDetail details) {
		return TriggerBuilder.newTrigger().forJob(details)
				.withSchedule(CronScheduleBuilder.cronSchedule(details.getJobDataMap().getString("cronExpression"))).build();
	}
	
	public void populateTable() {
		List<Long>statIdList = temRepo.getDistinctStatIds();
		for(Long statId : statIdList) {
			RegulatoryCode rc = new RegulatoryCode();
			String regCode = temRepo.findRegulatoryCodeByStatutoryId(statId);
			rc.setStatId(statId);
			rc.setRegulatoryCode(regCode);
			repo.save(rc);
		}
	}
}
