package com.logicladder.LLConnectorNew.app;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.datasource.AbstractDataSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.logicladder.LLConnectorNew.config.CommonConfig;
import com.logicladder.LLConnectorNew.config.ConfigFactory;
import com.logicladder.LLConnectorNew.models.IndustryModel;
import com.logicladder.LLConnectorNew.models.TenantExecutorMapping;
import com.logicladder.LLConnectorNew.utils.DateUtils;

@Component
public class LiveApplication {
	private static final Logger logger = LoggerFactory.getLogger(LiveApplication.class);
	private static final String XML_NAME = "DB_CONF.xml";

	protected int maxActive;
	protected int maxIdle;
	protected static AbstractDataSource mysqlDataSourceEnviro;
	protected static ThreadPoolExecutor executorService;

	public LiveApplication() {
		super();
	}

	public void initializeLiveApplication(List<TenantExecutorMapping> tenantExecutorMappingList) {
		logger.info("initializing Live Application");
		System.setProperty("user.timezone", "IST");
		String noOfThread = System.getProperty("no_of_thread", "1");
		int no_of_thread = Integer.parseInt(noOfThread);
		try {
			@SuppressWarnings("resource")
			ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(XML_NAME);
			mysqlDataSourceEnviro = (AbstractDataSource) context.getBean("mysqlDataSourceEnviro");
//			mysqlDataSourceEnviro.setMaxActive(maxActive);
//			mysqlDataSourceEnviro.setMaxIdle(maxIdle);
			logger.info("connection establish for mysql");
			// httpPostData = new HTTPPostData(config.postUrl, config.portNo,
			// config.schemeName);
			executorService = new ThreadPoolExecutor(no_of_thread, no_of_thread, // max
					// size
					2 * 60, // idle timeout
					TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(100));

			executorService.setRejectedExecutionHandler(new RejectedExecutionHandler() {
				@Override
				public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
					try {
						executor.getQueue().put(r);
					} catch (InterruptedException e) {
						return;
					}
				}
			});
		} catch (Throwable th) {
			logger.error("exception in loading Logicladder app", th);
		}
		
		List<Long> statIdList = new ArrayList<>();
		for (TenantExecutorMapping tenantExecutorMapping : tenantExecutorMappingList) {
			if(!statIdList.contains(tenantExecutorMapping.getStatutoryId())) {
				statIdList.add(tenantExecutorMapping.getStatutoryId());
			}

		}
		List<IndustryModel> industryList = fetchIndustryModel(statIdList);
		logger.debug("fetched industry size {}", industryList.size());
		for (IndustryModel llIndustry : industryList) {
			CommonConfig config = ConfigFactory.getInstance().getConfig(llIndustry.getRegulatoryCode());
			Date currentDate = DateUtils.getCurrentDateTime();
			String endTime = DateUtils.format(currentDate, "yyyy-MM-dd HH:mm:ss");
//			Executor llExecutor = ExecutorFactory
//					.getInstance(regulatoryCode, mysqlDataSourceEnviro, httpPostData, cache, config)
//					.getExecutor(llIndustry);
//			String lastUpdatedDate = llExecutor.manageNewIndustries(llIndustry.getIndustry());
//			String startTime = currentDate.getTime() - DateUtils.parse(lastUpdatedDate, "yyyy-MM-dd HH:mm:ss").getTime() < 300000 ? 
//					lastUpdatedDate : DateUtils.format(currentDate.getTime()-300000, "yyyy-MM-dd HH:mm:ss");
//			llIndustry.setStartTime(startTime);
//			llIndustry.setEndTime(endTime);
//			executorService.execute(llExecutor);
		}
	}

	private List<IndustryModel> fetchIndustryModel(List<Long> statIdList) {
		List<IndustryModel> industryModel = new ArrayList<IndustryModel>();
		
		
		return industryModel;

	}

}
