package com.logicladder.LLConnectorNew.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pcb_unit_mapping")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PCBUnit {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String spcbUnit;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pcbConfigId", referencedColumnName="id")
	private Tenant pcbConfiguration;
	
	@ManyToOne
	@JoinColumn(name = "unitId", referencedColumnName="id")
	private Unit unit;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getSpcbUnit() {
		return spcbUnit;
	}
	public void setSpcbUnit(String spcbUnit) {
		this.spcbUnit = spcbUnit;
	}
	public Unit getUnit() {
		return unit;
	}
	public void setUnit(Unit unit) {
		this.unit = unit;
	}
	public Tenant getPcbConfiguration() {
		return pcbConfiguration;
	}
	public void setPcbConfiguration(Tenant pcbConfiguration) {
		this.pcbConfiguration = pcbConfiguration;
	}
	
}