package com.logicladder.LLConnectorNew.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "meta_state")
public class State {
	/*@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)*/

	@Column(name="name")
	private String stateName;
	
	@Column(name="country_code")
	private String countryCode;
	
	@Id
	@Column(name="state_code")
	private String stateCode;
	
	@Column(name="short_code")
	private String shortCode;
	
	private String spcbLogo;
	private String spcbName;
	private String spcbDashboardStatus;

	public String getSpcbDashboardStatus() {
		return spcbDashboardStatus;
	}
	public void setSpcbDashboardStatus(String spcbDashboardStatus) {
		this.spcbDashboardStatus = spcbDashboardStatus;
	}
	public String getSpcbLogo() {
		return spcbLogo;
	}
	public String getSpcbName() {
		return spcbName;
	}
	public void setSpcbLogo(String spcbLogo) {
		this.spcbLogo = spcbLogo;
	}
	public void setSpcbName(String spcbName) {
		this.spcbName = spcbName;
	}
	public String getStateName() {
		return stateName;
	}
    public void setStateName(String stateName) {
		this.stateName = stateName;
	}
    public String getCountryCode() {
		return countryCode;
	}
    public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
    public String getStateCode() {
		return stateCode;
	}
    public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	@Override
	public String toString() {
		return "State [stateName=" + stateName + ", stateCode=" + stateCode + "]";
	}
	
}