package com.logicladder.LLConnectorNew.models;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "devices")
public class DeviceInfo  {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private long tenantId;
	private String name;
	private String manufactureId;
	private String timezone;
	private String entityId;
	private Long dataFrequency;
	private String tag;
	private String certification;
	private String certified;
	private String dataUploadMethod;
	private String diagnosticValues;
	private String diagnosticParameter;
	private Double noRatio;
	private Double no2Ratio;
	private String isNdirEnabled;
	private String status;
	private Long ndirId;
	private String liveStatus;
	private String simId;
	private String networkId;
	private String type;
	private String workingStatus;
	private String appId;
	private Long gatewayId;
	private String macOrImeiAddress;
	private String gatewayMake;
	private String gatewayModel;
	
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getWorkingStatus() {
		return workingStatus;
	}

	public void setWorkingStatus(String workingStatus) {
		this.workingStatus = workingStatus;
	}
	
	public String getSimId() {
		return simId;
	}

	public void setSimId(String simId) {
		this.simId = simId;
	}

	public void setLiveStatus(String liveStatus) {
		this.liveStatus = liveStatus;
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	
	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	private String serialNo;

	
	private Boolean isRemoteCalibrationStatus ;
	
	
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "tenantId", updatable=false,insertable=false)
	private Tenant tenant;
	
	public Boolean getIsRemoteCalibrationStatus() {
		return isRemoteCalibrationStatus;
	}

	public void setIsRemoteCalibrationStatus(Boolean isRemoteCalibrationStatus) {
		this.isRemoteCalibrationStatus = isRemoteCalibrationStatus;
	}

	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "modelId")
	private Model model;
	
	@JsonIgnore
	@ManyToOne(cascade = CascadeType.REFRESH)
	@JoinColumn(name = "calibratorModelId")
	private Model calibratorModel;

	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;

	@Temporal(TemporalType.TIMESTAMP)
	private Date lastDataAt;
	
	

	
	private String frtuType;

	public DeviceInfo() {

	}

	public DeviceInfo(String name) {

		super();
		this.name = name;
	}

	
	public Model getModel() {
		return model;
	}



	public void setModel(Model model) {
		this.model = model;
	}

	
	
	public Long getNdirId() {
		return ndirId;
	}

	public void setNdirId(Long ndirId) {
		this.ndirId = ndirId;
	}

	@JsonIgnore
	public Model getCalibratorModel() {
		return calibratorModel;
	}

	public void setCalibratorModel(Model calibratorModel) {
		this.calibratorModel = calibratorModel;
	}

	@JsonProperty
	public Long getId() {
		return id;
	}

	@JsonIgnore
	public void setId(Long id) {
		this.id = id;
	}

	public long getTenantId() {
		return tenantId;
	}

	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@JsonProperty
	public Date getCreatedDate() {
		return createdDate;
	}

	@JsonIgnore
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@JsonProperty
	public Date getDeletedDate() {
		return deletedDate;
	}

	@JsonIgnore
	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}
	
	
	public Date getLastDataAt() {
		return lastDataAt;
	}

	public void setLastDataAt(Date lastDataAt) {
		this.lastDataAt = lastDataAt;
	}

	
	
	public String getManufactureId() {
		return manufactureId;
	}

	public void setManufactureId(String manufactureId) {
		this.manufactureId = manufactureId;
	}


	@Override
	public DeviceInfo clone() throws CloneNotSupportedException {
		DeviceInfo d = (DeviceInfo) super.clone();
		d.setId(null);
		return d;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	

	public String getTenantName () {
		if (this.tenant==null)
			return null;
		
		return this.tenant.getName();
	}

	public Long getDataFrequency() {
		return dataFrequency;
	}

	public void setDataFrequency(Long dataFrequency) {
		this.dataFrequency = dataFrequency;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public String getCertified() {
		return certified;
	}

	public void setCertified(String certified) {
		this.certified = certified;
	}

	public String getCertification() {
		return certification;
	}

	public void setCertification(String certification) {
		this.certification = certification;
	}


	public String getDiagnosticParameter() {
		return diagnosticParameter;
	}

	public Double getNoRatio() {
		return noRatio;
	}

	public void setNoRatio(Double noRatio) {
		this.noRatio = noRatio;
	}

	public Double getNo2Ratio() {
		return no2Ratio;
	}

	public void setNo2Ratio(Double no2Ratio) {
		this.no2Ratio = no2Ratio;
	}


	public String getIsNdirEnabled() {
		return isNdirEnabled;
	}

	public void setIsNdirEnabled(String isNdirEnabled) {
		this.isNdirEnabled = isNdirEnabled;
	}

	public String getDataUploadMethod() {
		return dataUploadMethod;
	}

	public void setDataUploadMethod(String dataUploadMethod) {
		this.dataUploadMethod = dataUploadMethod;
	}


	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getFrtuType() {
		return frtuType;
	}

	public void setFrtuType(String frtuType) {
		this.frtuType = frtuType;
	}


	public String getLiveStatus() {
		return liveStatus;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public Long getGatewayId() {
		return gatewayId;
	}

	public void setGatewayId(Long gatewayId) {
		this.gatewayId = gatewayId;
	}
	
	public String getMacOrImeiAddress() {
		return macOrImeiAddress;
	}

	public void setMacOrImeiAddress(String macOrImeiAddress) {
		this.macOrImeiAddress = macOrImeiAddress;
	}

	public String getGatewayMake() {
		return gatewayMake;
	}

	public void setGatewayMake(String gatewayMake) {
		this.gatewayMake = gatewayMake;
	}

	public String getGatewayModel() {
		return gatewayModel;
	}

	public void setGatewayModel(String gatewayModel) {
		this.gatewayModel = gatewayModel;
	}
	
	@JsonProperty
	public long getAge() {
		if(createdDate != null) {
			return getDaysDifference(createdDate, new Date())/30;
		}
		return -1;
	}
	
	private long getDaysDifference(Date firstDate, Date secondDate) {
	    long diffInMillies = Math.abs(secondDate.getTime() - firstDate.getTime());
	    long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);
	    return diff;
	}

	public String getDiagnosticValues() {
		return diagnosticValues;
	}

	public void setDiagnosticValues(String diagnosticValues) {
		this.diagnosticValues = diagnosticValues;
	}

	public void setDiagnosticParameter(String diagnosticParameter) {
		this.diagnosticParameter = diagnosticParameter;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}
	
	
	
}
