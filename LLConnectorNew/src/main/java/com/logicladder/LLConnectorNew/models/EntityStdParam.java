package com.logicladder.LLConnectorNew.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "entity_stdParam")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EntityStdParam {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private long entityId;
	
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "stdParamId")
	private StdParam stdParam;

	@Column(name="units")
	private String unit;
	private Double min;
	private Double max;
    private String status = CommonStatus.ENABLED.getStatusCode();
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
    private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastDataAt;
	
	private Double resetValue;
	
	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("USERUnit")
	private String USERUnit;
	
	@Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date DeletedDate;
	private String sensorId;
	
	@Column(name = "reference_lines")
	private String referenceLines;

	
	public long getId() {
		return id;
	}
	
	public StdParam getStdParam() {
		return stdParam;
	}

	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String units) {
		this.unit = units;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}


	@JsonProperty("USERUnit")
	public String getUSERUnit() {
		return USERUnit;
	}

	public void setUSERUnit(String uSERUnit) {
		USERUnit = uSERUnit;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	

	public Double getResetValue() {
		return resetValue;
	}

	public void setResetValue(Double resetValue) {
		this.resetValue = resetValue;
	}

	public Date getDeletedDate() {
		return DeletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		DeletedDate = deletedDate;
	}

	//@JsonIgnore
	public Date getLastDataAt() {
		return lastDataAt;
	}

	public void setLastDataAt(Date lastDataAt) {
		this.lastDataAt = lastDataAt;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

	public String getReferenceLines() {
		return referenceLines;
	}

	public void setReferenceLines(String referenceLines) {
		this.referenceLines = referenceLines;
	}

}
