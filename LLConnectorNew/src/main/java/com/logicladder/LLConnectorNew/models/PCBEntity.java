package com.logicladder.LLConnectorNew.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pcb_entity_mapping")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PCBEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String spcbEntityId;

	@ManyToOne
	@JoinColumn(name = "pcbTenantId", referencedColumnName = "id")
	private StatutoryToTenant statTenant;

	private long entityId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSpcbEntityId() {
		return spcbEntityId;
	}

	public void setSpcbEntityId(String spcbEntityId) {
		this.spcbEntityId = spcbEntityId;
	}
	

	public StatutoryToTenant getStatTenant() {
		return statTenant;
	}

	public void setStatTenant(StatutoryToTenant statTenant) {
		this.statTenant = statTenant;
	}

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityId) {
		this.entityId = entityId;
	}

}
