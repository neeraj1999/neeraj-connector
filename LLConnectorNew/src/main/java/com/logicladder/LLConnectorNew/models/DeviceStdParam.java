package com.logicladder.LLConnectorNew.models;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "device_stdParam")
public class DeviceStdParam {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;

	private long deviceId;
	
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "stdParamId")
	private StdParam stdParam;

	@Column(name="units")
	private String unit;
	private Double min;
	private Double max;
    private String status = CommonStatus.ENABLED.getStatusCode();
	
	private Double calMinScale;
	private Double calMaxScale;
	
	private Long concValue;
	private Long displayOrder;

	@Column(name = "reference_lines")
	private String referenceLines;
	
	public Long getConcValue() {
		return concValue;
	}
	public void setConcValue(Long concValue) {
		this.concValue = concValue;
	}
	public Double getCalMinScale() {
		return calMinScale;
	}
	public void setCalMinScale(Double calMinScale) {
		this.calMinScale = calMinScale;
	}
	public Double getCalMaxScale() {
		return calMaxScale;
	}
	public void setCalMaxScale(Double calMaxScale) {
		this.calMaxScale = calMaxScale;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
    private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastDataAt;
	
	private Double resetValue;
	
	private Boolean persisted;

	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("USERUnit")
	private String USERUnit;
	@JsonProperty("CPCBUnit")
	private String CPCBUnit;
	@JsonProperty("SPCBUnit")
	private String SPCBUnit;
	
	@Temporal(TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date DeletedDate;
	
	private Boolean isStatutoryAlertEnabled;
	
	private Boolean remoteCalibrationFacility;

	private Boolean offlineAlert;
	
	private Boolean constantDataAlert;
	
	private String sensorId;

	
	public long getId() {
		return id;
	}
	public Boolean getPersisted() {
		return persisted;
	}





	public void setPersisted(Boolean persisted) {
		this.persisted = persisted;
	}


	public String getUnit() {
		return unit;
	}

	public void setUnit(String units) {
		this.unit = units;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Boolean getOfflineAlert() {
		return offlineAlert;
	}

	public void setOfflineAlert(Boolean offlineAlert) {
		this.offlineAlert = offlineAlert;
	}

	public String getUnitForUSerType(UserType userType) {
		switch (userType) {
		case USER:
			return getUSERUnit();
		case DEVICE:
			return getUnit();
		case CPCB :
			return getCPCBUnit();
		case SPCB :
			return getSPCBUnit();
		default:
			return getUnit();
		}
	}

	public boolean isConversion() {
		return getStdParam().isConversion();
	}

	public Double getMolWt() {
		return getStdParam().getMolWt();
	}

	@JsonProperty("USERUnit")
	public String getUSERUnit() {
		return USERUnit;
	}

	public void setUSERUnit(String uSERUnit) {
		USERUnit = uSERUnit;
	}

	@JsonProperty("CPCBUnit")
	public String getCPCBUnit() {
		return CPCBUnit;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Override
	public DeviceStdParam clone() throws CloneNotSupportedException {
		DeviceStdParam dsp = (DeviceStdParam) super.clone();
		dsp.setId(null);
		return dsp;
	}

	public Double getResetValue() {
		return resetValue;
	}

	public void setResetValue(Double resetValue) {
		this.resetValue = resetValue;
	}

	public void setCPCBUnit(String cPCBUnit) {
		CPCBUnit = cPCBUnit;
	}

	@JsonProperty("SPCBUnit")
	public String getSPCBUnit() {
		return SPCBUnit;
	}

	public void setSPCBUnit(String sPCBUnit) {
		SPCBUnit = sPCBUnit;
	}

	public Date getDeletedDate() {
		return DeletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		DeletedDate = deletedDate;
	}

	public Boolean getIsStatutoryAlertEnabled() {
		return isStatutoryAlertEnabled;
	}

	public void setIsStatutoryAlertEnabled(Boolean isStatutoryAlertEnabled) {
		this.isStatutoryAlertEnabled = isStatutoryAlertEnabled;
	}	
	
	public Boolean getRemoteCalibrationFacility() {
		return remoteCalibrationFacility;
	}

	public void setRemoteCalibrationFacility(Boolean remoteCalibrationFacility) {
		this.remoteCalibrationFacility = remoteCalibrationFacility;
	}
	
	//@JsonIgnore
	public Date getLastDataAt() {
		return lastDataAt;
	}

	public void setLastDataAt(Date lastDataAt) {
		this.lastDataAt = lastDataAt;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	public String getSensorId() {
		return sensorId;
	}

	public void setSensorId(String sensorId) {
		this.sensorId = sensorId;
	}

	public int compareTo(DeviceStdParam o) {
		if (this.getLastDataAt() == null || o.getLastDataAt() == null)
	        return 0;
	      return this.getLastDataAt().compareTo(o.getLastDataAt());
	}
	public Boolean getConstantDataAlert() {
		return constantDataAlert;
	}
	public void setConstantDataAlert(Boolean constantDataAlert) {
		this.constantDataAlert = constantDataAlert;
	}
	public Long getDisplayOrder() {
		return displayOrder;
	}
	public void setDisplayOrder(Long displayOrder) {
		this.displayOrder = displayOrder;
	}
	
	public String getReferenceLines() {
		return referenceLines;
	}
	public void setReferenceLines(String referenceLines) {
		this.referenceLines = referenceLines;
	}
	
	public StdParam getStdParam() {
		return stdParam;
	}

	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}
	
}
