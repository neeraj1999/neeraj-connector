package com.logicladder.LLConnectorNew.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "meta_virtual_std_params")
public class VirtualStdParam {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	private String type;				// The type of device to which this param belongs.
	private String name;				// The disaplay name like Total Active Energy Export
	@Column(name = "mtype")
	private String unit;				// unit for this param
	@JsonProperty("isCounter")
	private boolean isCounter;
	private String paramKey;			// key in our database like t_a_e_e;
	private String stdUnit;
	private String alertLabel;
	private boolean isAlertEnabled;
	private String label;
	private String paramMetric;
	private Boolean isConversion;
	private Double molWt;
	private String defaultStdUnit;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "stdParamId")
	private StdParam stdParam;
	
	public StdParam getStdParam() {
		return stdParam;
	}

	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}

	public VirtualStdParam() {
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("mtype")
	public String getUnit() {
		return unit;
	}

	@JsonProperty("mtype")
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@JsonProperty("isCounter")
	public boolean isCounter() {
		return isCounter;
	}

	@JsonIgnore
	public void setCounter(boolean isCounter) {
		this.isCounter = isCounter;
	}

	public String getParamKey() {
		return paramKey;
	}

	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}

	public String getStdUnit() {
		return stdUnit;
	}

	public void setStdUnit(String stdUnit) {
		this.stdUnit = stdUnit;
	}
	
	public String getAlertLabel() {
		return alertLabel;
	}

	public void setAlertLabel(String alertLabel) {
		this.alertLabel = alertLabel;
	}

	@JsonProperty("isAlertEnabled")
	public boolean isAlertEnabled() {
		return isAlertEnabled;
	}

	@JsonProperty("isAlertEnabled")
	public void setAlertEnabled(boolean isAlertEnabled) {
		this.isAlertEnabled = isAlertEnabled;
	}
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getParamMetric() {
		return paramMetric;
	}

	public void setParamMetric(String paramMetric) {
		this.paramMetric = paramMetric;
	}
	@JsonProperty("isConversion")
	public Boolean isConversion() {
		return isConversion;
	}
	@JsonProperty("isConversion")
	public void setIsConversion(Boolean isConversion) {
		this.isConversion = isConversion;
	}
	
	public Double getMolWt() {
		return molWt;
	}

	public void setMolWt(Double molWt) {
		this.molWt = molWt;
	}

	public String getDefaultStdUnit() {
		return defaultStdUnit;
	}

	public void setDefaultStdUnit(String defaultStdUnit) {
		this.defaultStdUnit = defaultStdUnit;
	}

}
