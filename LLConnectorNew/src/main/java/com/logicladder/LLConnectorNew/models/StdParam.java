package com.logicladder.LLConnectorNew.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "meta_std_params")
@JsonIgnoreProperties(ignoreUnknown = true)
public class StdParam {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "type")
	private String type;
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private String name; // The disaplay name like Total Active Energy Export
	@Column(name = "mtype")
	private String unit; // unit for this param
	@JsonProperty("isCounter")
	private boolean isCounter;
	private String paramKey; // key in our database like t_a_e_e;
	private String stdUnit;
	private String alertLabel;
	private boolean isAlertEnabled;
	private String label;
	private String paramMetric;
	private Boolean isConversion;
	private Double molWt;
	private String defaultStdUnit;
	private String paramColor;
	private Boolean showWidget;
	@Column(name = "downsample_agg")
	private String metricDownsampleAgg;
	@Column(name = "metric_agg")
	private String metricAgg;
	private String widgetName;
	private String appId;
	private boolean pickFirstOccurence;
	private String format;
	private String dataEntity;
	private String dataKeyAttr;
	@Column(name = "counter_code")
	private Integer counterCode;
	@JsonProperty("isNegativeValueAllowed")
	private boolean isNegativeValueAllowed;

	@Column(name = "isVirtual")
	private boolean virtual;
	
	@Column(name ="data_store_type")
    private String dataStoreType;
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stdParam", cascade = CascadeType.ALL)
	private List<DeviceStdParam> deviceStdParams = new ArrayList<DeviceStdParam>();

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stdParam", cascade = CascadeType.ALL)
	private List<EntityStdParam> entityStdParams = new ArrayList<EntityStdParam>();

	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "stdParam", cascade = CascadeType.ALL)
	private List<ModelStdParam> modelStdParams = new ArrayList<ModelStdParam>();

	@OneToMany(mappedBy = "stdParam")
	private List<VirtualStdParam> virtualStdParam;

	public StdParam() {
	}

	public StdParam(long id, String type, String name, String unit, String paramKey) {
		super();
		this.id = id;
		this.name = name;
		this.unit = unit;
		this.paramKey = paramKey;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("mtype")
	public String getUnit() {
		return unit;
	}

	@JsonProperty("mtype")
	public void setUnit(String unit) {
		this.unit = unit;
	}

	@JsonProperty("isCounter")
	public boolean isCounter() {
		return isCounter;
	}

	@JsonIgnore
	public void setCounter(boolean isCounter) {
		this.isCounter = isCounter;
	}

	public String getParamKey() {
		return paramKey;
	}

	public void setParamKey(String paramKey) {
		this.paramKey = paramKey;
	}

	public String getStdUnit() {
		return stdUnit;
	}

	public void setStdUnit(String stdUnit) {
		this.stdUnit = stdUnit;
	}

	public String getAlertLabel() {
		return alertLabel;
	}

	public void setAlertLabel(String alertLabel) {
		this.alertLabel = alertLabel;
	}

	@JsonProperty("isAlertEnabled")
	public boolean isAlertEnabled() {
		return isAlertEnabled;
	}

	@JsonProperty("isAlertEnabled")
	public void setAlertEnabled(boolean isAlertEnabled) {
		this.isAlertEnabled = isAlertEnabled;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getParamMetric() {
		return paramMetric;
	}

	public void setParamMetric(String paramMetric) {
		this.paramMetric = paramMetric;
	}

	@JsonProperty("isConversion")
	public Boolean isConversion() {
		return isConversion;
	}

	@JsonProperty("isConversion")
	public void setIsConversion(Boolean isConversion) {
		this.isConversion = isConversion;
	}

	public Double getMolWt() {
		return molWt;
	}

	public void setMolWt(Double molWt) {
		this.molWt = molWt;
	}

	public String getDefaultStdUnit() {
		return defaultStdUnit;
	}

	public void setDefaultStdUnit(String defaultStdUnit) {
		this.defaultStdUnit = defaultStdUnit;
	}

	public String getParamColor() {
		return paramColor;
	}

	public void setParamColor(String paramColor) {
		this.paramColor = paramColor;
	}

	public Boolean getShowWidget() {
		return showWidget;
	}

	public void setShowWidget(Boolean showWidget) {
		this.showWidget = showWidget;
	}

	public String getMetricDownsampleAgg() {
		return metricDownsampleAgg;
	}

	public void setMetricDownsampleAgg(String metricDownsampleAgg) {
		this.metricDownsampleAgg = metricDownsampleAgg;
	}

	public String getMetricAgg() {
		return metricAgg;
	}

	public void setMetricAgg(String metricAgg) {
		this.metricAgg = metricAgg;
	}

	public String getWidgetName() {
		return widgetName;
	}

	public void setWidgetName(String widgetName) {
		this.widgetName = widgetName;
	}

	public List<VirtualStdParam> getVirtualStdParam() {
		return virtualStdParam;
	}

	public void setVirtualStdParam(List<VirtualStdParam> virtualStdParam) {
		this.virtualStdParam = virtualStdParam;
	}

	@JsonProperty("virtual")
	public boolean isVirtual() {
		return virtual;
	}

	@JsonProperty("virtual")
	public void setVirtual(boolean virtual) {
		this.virtual = virtual;
	}
	
	public boolean isPickFirstOccurence() {
		return pickFirstOccurence;
	}

	public void setPickFirstOccurence(boolean pickFirstOccurence) {
		this.pickFirstOccurence = pickFirstOccurence;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getDataEntity() {
		return dataEntity;
	}

	public void setDataEntity(String dataEntity) {
		this.dataEntity = dataEntity;
	}

	public String getDataKeyAttr() {
		return dataKeyAttr;
	}

	public void setDataKeyAttr(String dataKeyAttr) {
		this.dataKeyAttr = dataKeyAttr;
	}

	public Integer getCounterCode() {
		return counterCode;
	}

	public void setCounterCode(Integer counterCode) {
		this.counterCode = counterCode;
	}

	public String getDataStoreType() {
		return dataStoreType;
	}

	public void setDataStoreType(String dataStoreType) {
		this.dataStoreType = dataStoreType;
	}

	public boolean getNegativeValueAllowed() {
		return isNegativeValueAllowed;
	}

	public void setNegativeValueAllowed(boolean isNegativeValueAllowed) {
		this.isNegativeValueAllowed = isNegativeValueAllowed;
	}

	
	
}
