package com.logicladder.LLConnectorNew.models;

import javax.persistence.Lob;

public class IndustryModel {
	
	private String industryId;
	private String pcbTenantId;
	private Long regulatoryId;
	private String regulatoryCode;
	private String siteId;
	private String siteEncryptionKey;
	private String siteName;
	private String siteAddress;
	private String siteCity;
	private String siteState;
	private String siteLat;
	private String siteLong;
	private String siteCountry;

	// Kerala specific variables
	private String industryCityState;
	@Lob
	private byte[] publicKeyFile;
	@Lob
	private byte[] privateKeyFile;
	private String serverKey;

	private String module;
	private String yuktixSerialNo;
	private String authorizationString;
	private String enviroSerialNo;
	private String startTime;
	private String endTime;
	private String lastUpdatedDate;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getSiteId() {
		return siteId;
	}

	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}

	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public String getYuktixSerialNo() {
		return yuktixSerialNo;
	}

	public void setYuktixSerialNo(String yuktixSerialNo) {
		this.yuktixSerialNo = yuktixSerialNo;
	}

	public String getAuthorizationString() {
		return authorizationString;
	}

	public void setAuthorizationString(String authorizationString) {
		this.authorizationString = authorizationString;
	}

	public String getEnviroSerialNo() {
		return enviroSerialNo;
	}

	public void setEnviroSerialNo(String enviroSerialNo) {
		this.enviroSerialNo = enviroSerialNo;
	}

	public String getIndustryCityState() {
		return industryCityState;
	}

	public void setIndustryCityState(String industryCityState) {
		this.industryCityState = industryCityState;
	}

	public byte[] getPublicKeyFile() {
		return publicKeyFile;
	}

	public void setPublicKeyFile(byte[] publicKeyFile) {
		this.publicKeyFile = publicKeyFile;
	}

	public byte[] getPrivateKeyFile() {
		return privateKeyFile;
	}

	public void setPrivateKeyFile(byte[] privateKeyFile) {
		this.privateKeyFile = privateKeyFile;
	}

	public String getServerKey() {
		return serverKey;
	}

	public void setServerKey(String serverKey) {
		this.serverKey = serverKey;
	}

	public String getSiteEncryptionKey() {
		return siteEncryptionKey;
	}

	public void setSiteEncryptionKey(String siteEncryptionKey) {
		this.siteEncryptionKey = siteEncryptionKey;
	}

	public String getSiteName() {
		return siteName;
	}

	public void setSiteName(String siteName) {
		this.siteName = siteName;
	}

	public String getSiteAddress() {
		return siteAddress;
	}

	public void setSiteAddress(String siteAddress) {
		this.siteAddress = siteAddress;
	}

	public String getSiteCity() {
		return siteCity;
	}

	public void setSiteCity(String siteCity) {
		this.siteCity = siteCity;
	}

	public String getSiteState() {
		return siteState;
	}

	public void setSiteState(String siteState) {
		this.siteState = siteState;
	}

	public String getSiteLat() {
		return siteLat;
	}

	public void setSiteLat(String siteLat) {
		this.siteLat = siteLat;
	}

	public String getSiteLong() {
		return siteLong;
	}

	public void setSiteLong(String siteLong) {
		this.siteLong = siteLong;
	}

	public String getSiteCountry() {
		return siteCountry;
	}

	public void setSiteCountry(String siteCountry) {
		this.siteCountry = siteCountry;
	}

	public String getPcbTenantId() {
		return pcbTenantId;
	}

	public void setPcbTenantId(String pcbTenantId) {
		this.pcbTenantId = pcbTenantId;
	}

	public String getIndustryId() {
		return industryId;
	}

	public void setIndustryId(String industryId) {
		this.industryId = industryId;
	}

	public Long getRegulatoryId() {
		return regulatoryId;
	}

	public void setRegulatoryId(Long regulatoryId) {
		this.regulatoryId = regulatoryId;
	}

	public String getRegulatoryCode() {
		return regulatoryCode;
	}

	public void setRegulatoryCode(String regulatoryCode) {
		this.regulatoryCode = regulatoryCode;
	}

	
}
