package com.logicladder.LLConnectorNew.models;

import java.util.Arrays;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "statutory_to_tenant")
public class StatutoryToTenant {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private Long statutoryId;
	private Long tenantId;

	@Temporal(TemporalType.TIMESTAMP)
	private Date assignedAt;
	private Integer assigned;
	private Integer sync_data_status;

	private String spcbTenantId;
	private String authorizationKey;
	private String signature;
	@Lob
	private byte[] publicKeyFile;
	@Lob
	private byte[] privateKeyFile;
	@Lob
	private byte[] serverKey;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSpcbTenantId() {
		return spcbTenantId;
	}

	public void setSpcbTenantId(String spcbTenantId) {
		this.spcbTenantId = spcbTenantId;
	}

	public String getAuthorizationKey() {
		return authorizationKey;
	}

	public void setAuthorizationKey(String authorizationKey) {
		this.authorizationKey = authorizationKey;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	public byte[] getPublicKeyFile() {
		return publicKeyFile;
	}

	public void setPublicKeyFile(byte[] publicKeyFile) {
		this.publicKeyFile = publicKeyFile;
	}

	public byte[] getPrivateKeyFile() {
		return privateKeyFile;
	}

	public void setPrivateKeyFile(byte[] privateKeyFile) {
		this.privateKeyFile = privateKeyFile;
	}

	public byte[] getServerKey() {
		return serverKey;
	}

	public void setServerKey(byte[] serverKey) {
		this.serverKey = serverKey;
	}

	public Long getStatutoryId() {
		return statutoryId;
	}

	public void setStatutoryId(Long statutoryId) {
		this.statutoryId = statutoryId;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public void setAssignedAt(Date assignedAt) {
		this.assignedAt = assignedAt;
	}

	public Date getAssignedAt() {
		return assignedAt;
	}

	public Integer getAssigned() {
		return assigned;
	}

	public void setAssigned(Integer assigned) {
		this.assigned = assigned;
	}

	public Integer getSync_data_status() {
		return sync_data_status;
	}

	public void setSync_data_status(Integer sync_data_status) {
		this.sync_data_status = sync_data_status;
	}

	@Override
	public String toString() {
		return "StatutoryToTenant [id=" + id + ", statutoryId=" + statutoryId
				+ ", tenantId=" + tenantId + ", assignedAt=" + assignedAt
				+ ", assigned=" + assigned + ", sync_data_status="
				+ sync_data_status + ", spcbTenantId=" + spcbTenantId
				+ ", authorizationKey=" + authorizationKey + ", signature="
				+ signature + ", publicKeyFile="
				+ Arrays.toString(publicKeyFile) + ", privateKeyFile="
				+ Arrays.toString(privateKeyFile) + ", serverKey="
				+ Arrays.toString(serverKey) + "]";
	}



}
