package com.logicladder.LLConnectorNew.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


@Entity
@Table(name="entities")
public class EntitiesInfo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private long tenantId;
	private String name;
	private String type;
	private String description;
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastDataAt;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable = false)
	private Date createdDate;
	@Temporal(TemporalType.TIMESTAMP)
	private Date deletedDate;
	private String timezone;

	public EntitiesInfo() {
		super();
	}

	public EntitiesInfo(long id, long tenantId, String name, String type, String description, Date lastDataAt,
			Date createdDate, Date deletedDate, String timezone) {
		super();
		this.id = id;
		this.tenantId = tenantId;
		this.name = name;
		this.type = type;
		this.description = description;
		this.lastDataAt = lastDataAt;
		this.createdDate = createdDate;
		this.deletedDate = deletedDate;
		this.timezone = timezone;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getTenantId() {
		return tenantId;
	}

	public void setTenantId(long tenantId) {
		this.tenantId = tenantId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getLastDataAt() {
		return lastDataAt;
	}

	public void setLastDataAt(Date lastDataAt) {
		this.lastDataAt = lastDataAt;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getDeletedDate() {
		return deletedDate;
	}

	public void setDeletedDate(Date deletedDate) {
		this.deletedDate = deletedDate;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

}
