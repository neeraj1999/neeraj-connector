package com.logicladder.LLConnectorNew.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "model")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Model {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY, cascade = { CascadeType.REFRESH })
	@JoinColumn(name = "makeId", nullable = false)
	private Make make;
	
//	@JsonManagedReference("model-params")
//	@OneToMany(mappedBy = "model", orphanRemoval = true, cascade = { CascadeType.ALL })
//	private List<ModelStdParam> params;

	private String name;
	
	private String appId;
	
	private Boolean remoteCalibrationFacility;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Make getMake() {
		return make;
	}

	public void setMake(Make make) {
		this.make = make;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
//	public List<ModelStdParam> getParams() {
//		return params;
//	}
//
//	public void setParams(List<ModelStdParam> params) {
//		this.params = params;
//	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}
		
	public Boolean getRemoteCalibrationFacility() {
		return remoteCalibrationFacility;
	}

	public void setRemoteCalibrationFacility(Boolean remoteCalibrationFacility) {
		this.remoteCalibrationFacility = remoteCalibrationFacility;
	}

	@Override
	public String toString() {
		String makeId = this.make != null ? String.valueOf(this.make.getId()) : null;
		return "Model [id=" + this.id + ", make=" + makeId + ", name=" + this.name + "]";
	}
}
