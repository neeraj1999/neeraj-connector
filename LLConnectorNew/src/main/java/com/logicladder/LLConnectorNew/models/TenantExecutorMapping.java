package com.logicladder.LLConnectorNew.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;

@Component
@Entity
@Table(name = "tenant_executor_mapping")
public class TenantExecutorMapping {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "tenant_id")
	private Long tenantId;
	
	@Column(name = "station_id")
	private Long stationId;
	
	@Column(name="regulator_id")
	private Long statutoryId;
	
	@Column(name = "executor_name")
	private String executorName;
	
	@Column(name = "cron_expression")
	private String cronExpression;
	
	@Column(name = "regulatory_code")
	private String regulatoryCode;

	public TenantExecutorMapping() {
		super();
	}

	public TenantExecutorMapping(Long id, Long tenantId, Long stationId, Long statutoryId, String executorName,
			String cronExpression, String regulatoryCode) {
		super();
		this.id = id;
		this.tenantId = tenantId;
		this.stationId = stationId;
		this.statutoryId = statutoryId;
		this.executorName = executorName;
		this.cronExpression = cronExpression;
		this.regulatoryCode = regulatoryCode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getTenantId() {
		return tenantId;
	}

	public void setTenantId(Long tenantId) {
		this.tenantId = tenantId;
	}

	public Long getStationId() {
		return stationId;
	}

	public void setStationId(Long stationId) {
		this.stationId = stationId;
	}

	public Long getStatutoryId() {
		return statutoryId;
	}

	public void setStatutoryId(Long statutoryId) {
		this.statutoryId = statutoryId;
	}

	public String getExecutorName() {
		return executorName;
	}

	public void setExecutorName(String executorName) {
		this.executorName = executorName;
	}

	public String getCronExpression() {
		return cronExpression;
	}

	public void setCronExpression(String cronExpression) {
		this.cronExpression = cronExpression;
	}

	public String getRegulatoryCode() {
		return regulatoryCode;
	}

	public void setRegulatoryCode(String regulatoryCode) {
		this.regulatoryCode = regulatoryCode;
	}


	
}
