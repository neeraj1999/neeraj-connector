package com.logicladder.LLConnectorNew.models;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="tenants")
public class Tenant {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String status;
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdDate;
	private String timezone;
	private long partnerId;
	private String type;
	private String liveStatus;
	@Column(name = "pcbConfig")
	private String pcbConfig;
	private long corporateId;
	@Column(name = "sub_app")
	private String subApp;
	private String isAlertEnabled;
	private String workingStatus;
	private String tempAppId;
	private String code;
	private String short_name;
	private String properties;
	private Integer connectedWithStat;
	private Integer cemsPresent;
	private Integer eqmsPresent;
	private Integer caaqmsPresent;
	private String token;
	@Column(name = "isDemo")
	private Boolean isDemoAccount;
	
	public Tenant() {
		super();
	}

	public Tenant(long id, String name, String status, Date createdDate, String timezone, long partnerId, String type,
			String liveStatus, String pcbConfig, long corporateId, String subApp, String isAlertEnabled,
			String workingStatus, String tempAppId, String code, String short_name, String properties,
			Integer connectedWithStat, Integer cemsPresent, Integer eqmsPresent, Integer caaqmsPresent, String token,
			Boolean isDemoAccount) {
		super();
		this.id = id;
		this.name = name;
		this.status = status;
		this.createdDate = createdDate;
		this.timezone = timezone;
		this.partnerId = partnerId;
		this.type = type;
		this.liveStatus = liveStatus;
		this.pcbConfig = pcbConfig;
		this.corporateId = corporateId;
		this.subApp = subApp;
		this.isAlertEnabled = isAlertEnabled;
		this.workingStatus = workingStatus;
		this.tempAppId = tempAppId;
		this.code = code;
		this.short_name = short_name;
		this.properties = properties;
		this.connectedWithStat = connectedWithStat;
		this.cemsPresent = cemsPresent;
		this.eqmsPresent = eqmsPresent;
		this.caaqmsPresent = caaqmsPresent;
		this.token = token;
		this.isDemoAccount = isDemoAccount;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public long getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(long partnerId) {
		this.partnerId = partnerId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getLiveStatus() {
		return liveStatus;
	}

	public void setLiveStatus(String liveStatus) {
		this.liveStatus = liveStatus;
	}

	public String getPcbConfig() {
		return pcbConfig;
	}

	public void setPcbConfig(String pcbConfig) {
		this.pcbConfig = pcbConfig;
	}

	public long getCorporateId() {
		return corporateId;
	}

	public void setCorporateId(long corporateId) {
		this.corporateId = corporateId;
	}

	public String getSubApp() {
		return subApp;
	}

	public void setSubApp(String subApp) {
		this.subApp = subApp;
	}

	public String getIsAlertEnabled() {
		return isAlertEnabled;
	}

	public void setIsAlertEnabled(String isAlertEnabled) {
		this.isAlertEnabled = isAlertEnabled;
	}

	public String getWorkingStatus() {
		return workingStatus;
	}

	public void setWorkingStatus(String workingStatus) {
		this.workingStatus = workingStatus;
	}

	public String getTempAppId() {
		return tempAppId;
	}

	public void setTempAppId(String tempAppId) {
		this.tempAppId = tempAppId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getShort_name() {
		return short_name;
	}

	public void setShort_name(String short_name) {
		this.short_name = short_name;
	}

	public String getProperties() {
		return properties;
	}

	public void setProperties(String properties) {
		this.properties = properties;
	}

	public Integer getConnectedWithStat() {
		return connectedWithStat;
	}

	public void setConnectedWithStat(Integer connectedWithStat) {
		this.connectedWithStat = connectedWithStat;
	}

	public Integer getCemsPresent() {
		return cemsPresent;
	}

	public void setCemsPresent(Integer cemsPresent) {
		this.cemsPresent = cemsPresent;
	}

	public Integer getEqmsPresent() {
		return eqmsPresent;
	}

	public void setEqmsPresent(Integer eqmsPresent) {
		this.eqmsPresent = eqmsPresent;
	}

	public Integer getCaaqmsPresent() {
		return caaqmsPresent;
	}

	public void setCaaqmsPresent(Integer caaqmsPresent) {
		this.caaqmsPresent = caaqmsPresent;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Boolean getIsDemoAccount() {
		return isDemoAccount;
	}

	public void setIsDemoAccount(Boolean isDemoAccount) {
		this.isDemoAccount = isDemoAccount;
	}
	
}
