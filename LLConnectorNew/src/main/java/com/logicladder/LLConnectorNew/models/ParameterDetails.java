package com.logicladder.LLConnectorNew.models;



public class ParameterDetails {
	protected int IndustryId;
	protected int StationId;
	protected int oemDeviceID;
	protected String deviceName;
	protected String serialNo;
	protected String parameter;
	protected String metric;
	protected String unit;
	protected int deviceId;
	protected int stdParamId;
	protected boolean isConversion;
	protected String mtype;
	protected double conversionFactor;
	protected double conversionConstant;
	protected int si;
	protected boolean ConsiderMolWt;
	protected Double molWt;
	protected String lattitude;
	protected String Longitude;
	protected String lastUpdatedDate;
	protected String analyzerID;
	protected String maxLimit;
	protected String minLimit;
	protected String PCBParameter;
	protected int parameterIdAtEnviro;
	protected String DiagParam;
	protected String massType;
	protected String deviceParamId;
	protected String SPCBUnitName;

	protected String industryName;
	protected String codeOfParam;
	
	public String getIndustryName() {
		return industryName;
	}

	public void setIndustryName(String industryName) {
		this.industryName = industryName;
	}

	public String getCodeOfParam() {
		return codeOfParam;
	}

	public void setCodeOfParam(String codeOfParam) {
		this.codeOfParam = codeOfParam;
	}

	public String getMassType() {
		return massType;
	}

	public void setMassType(String massType) {
		this.massType = massType;
	}
	
	public ParameterDetails(){
		
	}
	
    public String getSPCBUnit() {
		return SPCBUnit;
	}

	public void setSPCBUnit(String sPCBUnit) {
		SPCBUnit = sPCBUnit;
	}

	public String getPcbDeviceId() {
		return pcbDeviceId;
	}

	public void setPcbDeviceId(String pcbDeviceId) {
		this.pcbDeviceId = pcbDeviceId;
	}

	public String getPcbParameterId() {
		return pcbParameterId;
	}

	public void setPcbParameterId(String pcbParameterId) {
		this.pcbParameterId = pcbParameterId;
	}

	public RegulatorUnit getSourceUnit() {
		return SourceUnit;
	}

	public void setSourceUnit(RegulatorUnit sourceUnit) {
		SourceUnit = sourceUnit;
	}

	public RegulatorUnit getSPCBDestinationUnit() {
		return SPCBDestinationUnit;
	}

	public void setSPCBDestinationUnit(RegulatorUnit sPCBDestinationUnit) {
		SPCBDestinationUnit = sPCBDestinationUnit;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSpcbTenantName() {
		return spcbTenantName;
	}

	public void setSpcbTenantName(String spcbTenantName) {
		this.spcbTenantName = spcbTenantName;
	}

	public String getSpcbStationName() {
		return spcbStationName;
	}

	public void setSpcbStationName(String spcbStationName) {
		this.spcbStationName = spcbStationName;
	}
	public String getSPCBUnitName() {
		return SPCBUnitName;
	}

	public void setSPCBUnitName(String sPCBUnitName) {
		SPCBUnitName = sPCBUnitName;
	}


	private String SPCBUnit;
    private String pcbDeviceId;
    private String pcbParameterId;
	private RegulatorUnit SourceUnit;
    private RegulatorUnit SPCBDestinationUnit;

	private String address;
    private String spcbTenantName;
    protected String spcbStationName; 
    
    
    protected String make;
    protected String model;
    protected String contactName;
    protected String contactEmail;
    protected String contactNo;
	
	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

    
	public String getDiagParam() {
		return DiagParam;
	}

	public void setDiagParam(String diagParam) {
		DiagParam = diagParam;
	}
	
	public int getParameterIdAtEnviro() {
		return parameterIdAtEnviro;
	}

	public void setParameterIdAtEnviro(int parameterIdAtEnviro) {
		this.parameterIdAtEnviro = parameterIdAtEnviro;
	}



	public String getMaxLimit() {
		return maxLimit;
	}

	public void setMaxLimit(String maxLimit) {
		this.maxLimit = maxLimit;
	}

	public String getMinLimit() {
		return minLimit;
	}

	public void setMinLimit(String minLimit) {
		this.minLimit = minLimit;
	}

	public double getConversionFactor() {
		return conversionFactor;
	}
	
	public void setConversionFactor(double conversionFactor) {
		this.conversionFactor = conversionFactor;
	}
	
	public Double getMolWt() {
		return molWt;
	}
	
	public void setMolWt(Double molWt) {
		this.molWt = molWt;
	}
	
	public double getConversionConstant() {
		return conversionConstant;
	}
	
	public void setConversionConstant(double conversionConstant) {
		this.conversionConstant = conversionConstant;
	}
	
	public int getSi() {
		return si;
	}
	
	public void setSi(int si) {
		this.si = si;
	}
		
	public int getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(int deviceId) {
		this.deviceId = deviceId;
	}

	public int getStdParamId() {
		return stdParamId;
	}

	public void setStdParamId(int stdParamId) {
		this.stdParamId = stdParamId;
	}

	public int getIndustryId() {
		return IndustryId;
	}

	public void setIndustryId(int industryId) {
		IndustryId = industryId;
	}

	public int getStationId() {
		return StationId;
	}

	public void setStationId(int stationId) {
		StationId = stationId;
	}

	public int getOemDeviceID() {
		return oemDeviceID;
	}

	public void setOemDeviceID(int oemDeviceID) {
		this.oemDeviceID = oemDeviceID;
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public String getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public String getMetric() {
		return metric;
	}

	public void setMetric(String metric) {
		this.metric = metric;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	/*
	 * public String getSqliteTableName() { return sqliteTableName; }
	 * 
	 * public void setSqliteTableName(String sqliteTableName) {
	 * this.sqliteTableName = sqliteTableName; }
	 */
	
	
	public String getDeviceParamId() {
		return deviceParamId;
	}

	public void setDeviceParamId(String deviceParamId) {
		this.deviceParamId = deviceParamId;
	}

	@Override
	public String toString() {
		return "ParameterDetails [IndustryId=" + IndustryId + ", StationId=" + StationId + ", oemDeviceID=" + oemDeviceID + ", deviceName="
				+ deviceName + ", serialNo=" + serialNo + ", parameter=" + parameter + ", metric=" + metric + ", unit=" + unit + ", deviceId=" + deviceId
				+ ", stdParamId=" + stdParamId + ", isConversion=" + isConversion + ", mtype=" + mtype
				+ ", conversionFactor=" + conversionFactor + ", conversionConstant=" + conversionConstant + ", si=" + si + ", ConsiderMolWt=" + ConsiderMolWt
				+ ", molWt=" + molWt + ", lattitude=" + lattitude + ", Longitude=" + Longitude + ", lastUpdatedDate=" + lastUpdatedDate + ", analyzerID="
				+ analyzerID + ", maxLimit=" + maxLimit + ", minLimit=" + minLimit + ", codeOfParam=" + PCBParameter + "]";
	}

	public boolean getIsConversion() {
		return isConversion;
	}

	public void setIsConversion(boolean isConversion) {
		this.isConversion = isConversion;
	}

	public String getMtype() {
		return mtype;
	}

	public void setMtype(String mtype) {
		this.mtype = mtype;
	}

	public boolean isConsiderMolWt() {
		return ConsiderMolWt;
	}

	public void setConsiderMolWt(boolean considerMolWt) {
		ConsiderMolWt = considerMolWt;
	}

	public String getLattitude() {
		return lattitude;
	}

	public void setLattitude(String lattitude) {
		this.lattitude = lattitude;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getLastUpdatedDate() {
		return lastUpdatedDate;
	}

	public void setLastUpdatedDate(String lastUpdatedDate) {
		this.lastUpdatedDate = lastUpdatedDate;
	}

	public String getAnalyzerID() {
		return analyzerID;
	}

	public void setAnalyzerID(String analyzerID) {
		this.analyzerID = analyzerID;
	}

	public String getPCBParameter() {
		return PCBParameter;
	}

	public void setPCBParameter(String codeOfParam) {
		this.PCBParameter = codeOfParam;
	}

/*
	 (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 
	@Override
	public int hashCode() {
		throw new RuntimeException("We should not reach here");	
	}
*/


}
