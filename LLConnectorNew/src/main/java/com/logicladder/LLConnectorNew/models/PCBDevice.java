package com.logicladder.LLConnectorNew.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pcb_device_mapping")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PCBDevice {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String spcbDeviceId;
	
	@ManyToOne
	@JoinColumn(name = "parameterId", referencedColumnName="id")
	private StdParam stdParam;

	@ManyToOne
	@JoinColumn(name="spcbEntityId", referencedColumnName="id")
	private PCBEntity pcbEntity; 
	
	@ManyToOne
	@JoinColumn(name = "spcbParameterId", referencedColumnName="id")
	private PCBParameter pcbParameter;

	@ManyToOne
	@JoinColumn(name = "parameterUnit", referencedColumnName="id")
	private Unit unit;
	
	private long deviceId;
	
	@ManyToOne
	@JoinColumn(name = "spcbParameterUnit", referencedColumnName="id")
	private PCBUnit pcbUnit;
	
	public Unit getUnit() {
		return unit;
	}

	public void setUnit(Unit unit) {
		this.unit = unit;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSpcbDeviceId() {
		return spcbDeviceId;
	}

	public void setSpcbDeviceId(String spcbDeviceId) {
		this.spcbDeviceId = spcbDeviceId;
	}

	
	public PCBParameter getPcbParameter() {
		return pcbParameter;
	}

	public void setPcbParameter(PCBParameter pcbParameter) {
		this.pcbParameter = pcbParameter;
	}


	public StdParam getStdParam() {
		return stdParam;
	}

	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}

	public PCBEntity getPcbEntity() {
		return pcbEntity;
	}

	public void setPcbEntity(PCBEntity pcbEntity) {
		this.pcbEntity = pcbEntity;
	}

	public PCBUnit getPcbUnit() {
		return pcbUnit;
	}

	public void setPcbUnit(PCBUnit pcbUnit) {
		this.pcbUnit = pcbUnit;
	}

	public long getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(long deviceId) {
		this.deviceId = deviceId;
	}
	
	
}

