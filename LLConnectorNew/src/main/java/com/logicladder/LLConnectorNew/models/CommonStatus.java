package com.logicladder.LLConnectorNew.models;

public enum CommonStatus {
	AWAITED("AWAITED"), 
	ENABLED("ENABLED"), 
	DISABLED("DISABLED"), 
	DEMO("DEMO"), 
	TRIAL("TRIAL"),
	GRACE("GRACE"),
	
	ON("ON"),
	SEASONAL_SHUTDOWN("SEASONAL SHUTDOWN"),
	PERMANENT_SHUTDOWN("PERMANENT SHUTDOWN"),
	UNDER_MAINTENANCE("UNDER MAINTENANCE"),
	CLOSED_BY_REGULATOR("CLOSED BY REGULATOR"),
	
	LIVE("LIVE"),
	DELAYED("DELAYED"),
	OFFLINE("OFFLINE"),
	NODATA("NODATA"),
	
	INVITED("INVITED"), 
	DELETED("DELETED"),
	ACTIVE("ACTIVE"),
	PENDING("PENDING");

	private String statusCode;

	private CommonStatus(String s) {
		statusCode = s;
	}

	public String getStatusCode() {
		return statusCode;
	}

}
