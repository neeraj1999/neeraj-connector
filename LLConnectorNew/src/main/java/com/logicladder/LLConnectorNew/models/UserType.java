package com.logicladder.LLConnectorNew.models;

public enum UserType {
	SPCB,
	CPCB,
	USER,
	DEVICE;
}
