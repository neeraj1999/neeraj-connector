package com.logicladder.LLConnectorNew.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "meta_units")
public class Unit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	private String name;
	private String type;
	private double conversionFactor;
	private double conversionConstant;
	private boolean si;
	private boolean considerMolWt;
	private String appId;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getConversionFactor() {
		return conversionFactor;
	}
	public void setConversionFactor(double conversionFactor) {
		this.conversionFactor = conversionFactor;
	}
	public double getConversionConstant() {
		return conversionConstant;
	}
	public void setConversionConstant(double conversionConstant) {
		this.conversionConstant = conversionConstant;
	}
	public boolean isSi() {
		return si;
	}
	public void setSi(boolean si) {
		this.si = si;
	}
	public long getId() {
		return id;
	}
	public boolean isConsiderMolWt() {
		return considerMolWt;
	}
	public void setConsiderMolWt(boolean considerMolWt) {
		this.considerMolWt = considerMolWt;
	}
	
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	@Override
	public String toString() {
		return "Unit [id=" + id + ", name=" + name + ", type=" + type + ", conversionFactor=" + conversionFactor
				+ ", conversionConstant=" + conversionConstant + ", si=" + si + ", considerMolWt=" + considerMolWt
				+ "]";
	}
	
}
