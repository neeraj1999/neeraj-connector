package com.logicladder.LLConnectorNew.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "regulatory_code")
public class RegulatoryCode {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	@Column(name = "regulatorId")
	private long statId;
	@Column(name = "regulatoryCode")
	private String regulatoryCode;

	public RegulatoryCode() {
		super();
		// TODO Auto-generated constructor stub
	}

	public RegulatoryCode(long id, long statId, String regulatoryCode) {
		super();
		this.id = id;
		this.statId = statId;
		this.regulatoryCode = regulatoryCode;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getStatId() {
		return statId;
	}

	public void setStatId(long statId) {
		this.statId = statId;
	}

	public String getRegulatoryCode() {
		return regulatoryCode;
	}

	public void setRegulatoryCode(String regulatoryCode) {
		this.regulatoryCode = regulatoryCode;
	}

}
