package com.logicladder.LLConnectorNew.models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "pcb_parameter_mapping")
@JsonIgnoreProperties(ignoreUnknown = true)
public class PCBParameter {

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long id;
	private String spcbParameterId;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pcbConfigId", referencedColumnName="id")
	private Tenant pcbConfiguration;
	
	@ManyToOne
	@JoinColumn(name = "parameterId", referencedColumnName="id")
	private StdParam stdParam;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public StdParam getStdParam() {
		return stdParam;
	}
	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}
	public String getSpcbParameterId() {
		return spcbParameterId;
	}
	public void setSpcbParameterId(String spcbParameterId) {
		this.spcbParameterId = spcbParameterId;
	}
	public Tenant getPcbConfiguration() {
		return pcbConfiguration;
	}
	public void setPcbConfiguration(Tenant pcbConfiguration) {
		this.pcbConfiguration = pcbConfiguration;
	}
	
}

