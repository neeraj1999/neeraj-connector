package com.logicladder.LLConnectorNew.models;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "model_std_param")
@JsonIgnoreProperties(ignoreUnknown=true)
public class ModelStdParam {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;


	@Column(name = "modelId")
	private long modelId;
	
	@ManyToOne(cascade = {CascadeType.REFRESH})
	@JoinColumn(name = "stdParamId")
	private StdParam stdParam;

	@Column(name="units")
	private String unit;
	private Double min;
	private Double max;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(updatable=false)
    private Date createdDate;
	
	private Double resetValue;

	public void setId(Long id) {
		this.id = id;
	}
	@JsonProperty("USERUnit")
	private String USERUnit;
	@JsonProperty("CPCBUnit")
	private String CPCBUnit;
	@JsonProperty("SPCBUnit")
	private String SPCBUnit;
	
	public long getId() {
		return id;
	}


	public StdParam getStdParam() {
		return stdParam;
	}

	public void setStdParam(StdParam stdParam) {
		this.stdParam = stdParam;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String units) {
		this.unit = units;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}


	@Override
	public String toString() {
		return "ModelStdParam [id=" + id + ", modelId=" + modelId + ", stdParam=" + stdParam + ", unit=" + unit
				+ ", min=" + min + ", max=" + max + ", createdDate=" + createdDate + ", resetValue=" + resetValue
				+ ", USERUnit=" + USERUnit + ", CPCBUnit=" + CPCBUnit + ", SPCBUnit=" + SPCBUnit + "]";
	}


	@JsonProperty("USERUnit")
	public String getUSERUnit() {
		return USERUnit;
	}

	public void setUSERUnit(String uSERUnit) {
		USERUnit = uSERUnit;
	}

	@JsonProperty("CPCBUnit")
	public String getCPCBUnit() {
		return CPCBUnit;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	
	@Override
	public ModelStdParam clone() throws CloneNotSupportedException {
		ModelStdParam dsp = (ModelStdParam) super.clone();
		dsp.setId(null);
		return dsp;
	}

	public Double getResetValue() {
		return resetValue;
	}

	public void setResetValue(Double resetValue) {
		this.resetValue = resetValue;
	}

	public void setCPCBUnit(String cPCBUnit) {
		CPCBUnit = cPCBUnit;
	}

	@JsonProperty("SPCBUnit")
	public String getSPCBUnit() {
		return SPCBUnit;
	}

	public void setSPCBUnit(String sPCBUnit) {
		SPCBUnit = sPCBUnit;
	}

}
