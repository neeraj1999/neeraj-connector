package com.logicladder.LLConnectorNew.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "meta_country")
public class Country {
	@Column(name = "country_name")
	private String countryName;

	@Id
	@Column(name = "country_code")
	private String countryCode;

	@Column(name = "timezone")
	private String timezone;

	public Country() {
		super();
	}

	public Country(String countryName, String countryCode, String timezone) {
		super();
		this.countryName = countryName;
		this.countryCode = countryCode;
		this.timezone = timezone;
	}

	public String getCountryName() {
		return countryName;
	}

	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	@Override
	public String toString() {
		return "Country [countryName=" + countryName + ", countryCode=" + countryCode + "]";
	}

}
