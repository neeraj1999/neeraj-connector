package com.logicladder.LLConnectorNew.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PcbApiResponseDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3994700781422603192L;
	
	private String tenantId;
	private String entityId;
	private String requestStatus;
	private String apiResponseMessage;
	private String apiRequestedTime;
	private String forTimeStamp;
	private String fileName;
	private String requestedJson;
	private String apiHeaders;
	private String statutoryId;
	private String response;
	private String parameterIdsCommaSeperatedInRequest;
	private String parametersIncludedInRequest;
	private List<FileAttachmentDTO> attachments;
	
	public String getParameterIdsCommaSeperatedInRequest() {
		return parameterIdsCommaSeperatedInRequest;
	}
	public String getParametersIncludedInRequest() {
		return parametersIncludedInRequest;
	}
	public void setParameterIdsCommaSeperatedInRequest(String parameterIdsCommaSeperatedInRequest) {
		this.parameterIdsCommaSeperatedInRequest = parameterIdsCommaSeperatedInRequest;
	}
	public void setParametersIncludedInRequest(String parametersIncludedInRequest) {
		this.parametersIncludedInRequest = parametersIncludedInRequest;
	}
	@Override
	public String toString() {
		return "PcbApiResponseDTO [tenantId=" + tenantId + ", entityId=" + entityId + ", requestStatus=" + requestStatus + ", apiRequestedTime="
				+ apiRequestedTime + ", forTimeStamp=" + forTimeStamp + ", fileName=" + fileName + ", apiResponseMessage=" + apiResponseMessage
				+ ", requestedJson=" + requestedJson + ", apiHeaders=" + apiHeaders + ", statutoryId=" + statutoryId + ", parameterIdsCommaSeperatedInRequest="
				+ parameterIdsCommaSeperatedInRequest + ", parametersIncludedInRequest=" + parametersIncludedInRequest + ", attachments=" + attachments + "]";
	}
	
	public String getTenantId() {
		return tenantId;
	}
	public String getEntityId() {
		return entityId;
	}
	
	public String getApiRequestedTime() {
		return apiRequestedTime;
	}
	public String getForTimeStamp() {
		return forTimeStamp;
	}
	public String getFileName() {
		return fileName;
	}
	public String getRequestedJson() {
		return requestedJson;
	}
	public String getApiHeaders() {
		return apiHeaders;
	}
	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}
	
	public String getStatutoryId() {
		return statutoryId;
	}
	
	public void setStatutoryId(String statutoryId) {
		this.statutoryId = statutoryId;
	}
	
	public void setApiRequestedTime(String apiRequestedTime) {
		this.apiRequestedTime = apiRequestedTime;
	}
	public void setForTimeStamp(String forTimeStamp) {
		this.forTimeStamp = forTimeStamp;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public void setRequestedJson(String requestedJson) {
		this.requestedJson = requestedJson;
	}
	public void setApiHeaders(String apiHeaders) {
		this.apiHeaders = apiHeaders;
	}
	public List<FileAttachmentDTO> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<FileAttachmentDTO> attachments) {
		this.attachments = attachments;
	}
	
	public void addAttachment(FileAttachmentDTO attachment){
		if(attachments == null){
			attachments = new ArrayList<PcbApiResponseDTO.FileAttachmentDTO>();
		}
		attachments.add(attachment);
	}
	
	public String getApiResponseMessage() {
		return apiResponseMessage;
	}
	public void setApiResponseMessage(String apiResponseMessage) {
		this.apiResponseMessage = apiResponseMessage;
	}
	
	public String getRequestStatus() {
		return requestStatus;
	}
	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}
	public String getResponse() {
		return response;
	}
	public void setResponse(String response) {
		this.response = response;
	}

	
	public static class FileAttachmentDTO {
		
		private String name;
		private String contentType;
		private byte[] content;
		
		
		public FileAttachmentDTO(String name, String contentType, byte[] content) {
			super();
			this.name = name;
			this.contentType = contentType;
			this.content = content;
		}
		
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getContentType() {
			return contentType;
		}
		public void setContentType(String contentType) {
			this.contentType = contentType;
		}
		public byte[] getContent() {
			return content;
		}
		public void setContent(byte[] content) {
			this.content = content;
		}
	}
	
}