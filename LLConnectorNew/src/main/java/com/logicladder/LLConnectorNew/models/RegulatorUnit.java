package com.logicladder.LLConnectorNew.models;

public class RegulatorUnit {

	private long id;
	private String name;
	private String type;
	private double conversionFactor;
	private double conversionConstant;
	private boolean si;
	private boolean considerMolWt;
	private String SPCBUnit;
	private Double molWt;
	private String srcUnit;
	
	
	
	
	public String getSPCBUnit() {
		return SPCBUnit;
	}
	public void setSPCBUnit(String sPCBUnit) {
		SPCBUnit = sPCBUnit;
	}
	public String getSrcUnit() {
		return srcUnit;
	}
	public void setSrcUnit(String srcUnit) {
		this.srcUnit = srcUnit;
	}
	public Double getMolWt() {
		return molWt;
	}
	public void setMolWt(Double molWt) {
		this.molWt = molWt;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getConversionFactor() {
		return conversionFactor;
	}
	public void setConversionFactor(double conversionFactor) {
		this.conversionFactor = conversionFactor;
	}
	public double getConversionConstant() {
		return conversionConstant;
	}
	public void setConversionConstant(double conversionConstant) {
		this.conversionConstant = conversionConstant;
	}
	public boolean getSi() {
		return si;
	}
	public void setSi(boolean si) {
		this.si = si;
	}
	public boolean isConsiderMolWt() {
		return considerMolWt;
	}
	public void setConsiderMolWt(boolean considerMolWt) {
		this.considerMolWt = considerMolWt;
	}
	@Override
	public String toString() {
		return "Unit [id=" + id + ", name=" + name + ", type=" + type
				+ ", conversionFactor=" + conversionFactor
				+ ", conversionConstant=" + conversionConstant + ", si=" + si
				+ ", considerMolWt=" + considerMolWt + ", SPCBUnit=" + SPCBUnit
				+ ", molWt=" + molWt + ", srcUnit=" + srcUnit + "]";
	}
	 	
}
