package com.logicladder.LLConnectorNew.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.logicladder.LLConnectorNew.models.TenantExecutorMapping;
import com.logicladder.LLConnectorNew.repository.TenantExecutorMappingRepo;

@Service
@Component
public class TenantExecutorMappingService {

	@Autowired
	TenantExecutorMappingRepo temRepo;
	
	public List<TenantExecutorMapping> getAll() {
		return temRepo.findAll();
	}
	
	public List<String> getListOfCron(){
		return temRepo.getDistinctCronExpression();
	}
	
//	public TenantExecutorMapping saveAll(@RequestBody TenantExecutorMapping tem){
//		return temRepo.save(tem);
//	}
	
}
