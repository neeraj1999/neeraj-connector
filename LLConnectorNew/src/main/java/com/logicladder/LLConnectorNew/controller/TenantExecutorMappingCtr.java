package com.logicladder.LLConnectorNew.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import com.logicladder.LLConnectorNew.models.TenantExecutorMapping;
import com.logicladder.LLConnectorNew.repository.TenantExecutorMappingRepo;
import com.logicladder.LLConnectorNew.service.TenantExecutorMappingService;

@Controller
@Component
public class TenantExecutorMappingCtr {

	@Autowired
	TenantExecutorMappingService service;

	@Autowired
	TenantExecutorMappingRepo repo;
	
	public List<TenantExecutorMapping> get() {
		return this.service.getAll();
	}
//
//	public TenantExecutorMapping Post(@RequestBody TenantExecutorMapping tem) {
//		return this.service.saveAll(tem);
//
//	}
	
	public List<String> getCron(){
		return this.repo.getDistinctCronExpression();
	}
}
