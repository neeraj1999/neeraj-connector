package com.logicladder.LLConnectorNew.job;

import java.util.List;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.logicladder.LLConnectorNew.app.LiveApplication;
import com.logicladder.LLConnectorNew.models.TenantExecutorMapping;


public class RunLiveApplicationJob implements Job {
	private static final Logger logger = LoggerFactory.getLogger(RunLiveApplicationJob.class);

	@Autowired
	LiveApplication liveApp;

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		logger.info("started RunLiveApplicationJob");
		JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
		liveApp.initializeLiveApplication((List<TenantExecutorMapping>) jobDataMap.get("mappingList"));
	}

}
